﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CalTest
{
    public class Program
    {//Trivial
        static string[] Scopes = { CalendarService.Scope.CalendarReadonly, CalendarService.Scope.Calendar };
        static string ApplicationName = "IGS Fiat";

        public static Boolean CreateEvents(String address, List<Int32> frms, List<String> acts, List<String> msgs, String startAt, String recipe, String secretjson,
            List<String> subscribers)
        {
            CalendarService service = GetCalService(secretjson);
            DateTime dsa = DateTime.Parse(startAt);
            for (int i = 0; i < frms.Count; i++)
            {// 
                Event e = new Event()
                {
                    Summary = (msgs[i]!= null && msgs[i]!="")?msgs[i]:acts[i],
                    Location = "IGS "+address,
                    Start = new EventDateTime()
                    {
                        DateTime = dsa.AddSeconds(frms[i]),
                        TimeZone = "Europe/London"
                    },
                    End = new EventDateTime(){
                        DateTime = dsa.AddSeconds(frms[i]+30*60),
                        TimeZone = "Europe/London"
                    },
                    Description = "Part of Protocol "+recipe,
                };
                if (subscribers.Count > 0)
                    e.Attendees = new List<EventAttendee>();
                foreach (String s in subscribers)
                {
                    e.Attendees.Add(new EventAttendee()
                    {
                        Email = s
                    });
                }
                String calendarId = "primary";
                EventsResource.InsertRequest request = service.Events.Insert(e, calendarId);
                try {
                    Event createdEvent = request.Execute();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return false;
                }
            }
            return true;
        }

        static CalendarService GetCalService(String secretjson)
        {
            UserCredential credential;
            using (var stream =
                new FileStream(secretjson, FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/calendar-dotnet-quickstart");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Calendar API service.
            CalendarService service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            return service;
        }
        public static void NukeCal(String secretjson)
        {
            CalendarService service = GetCalService(secretjson);
            // Define parameters of request.
            EventsResource.ListRequest request = service.Events.List("primary");
            request.TimeMin = DateTime.Now;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = 100;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            // List events.
            Events events = request.Execute();
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    if (eventItem.Description!=null && eventItem.Description.StartsWith("Part of Protocol"))
                        service.Events.Delete("primary", eventItem.Id).Execute();
                }
            }
        }
     
        static void Main(string[] args)
        {
            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/calendar-dotnet-quickstart");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Calendar API service.
            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define parameters of request.
            EventsResource.ListRequest request = service.Events.List("primary");
            request.TimeMin = DateTime.Now;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = 10;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            // List events.
            Events events = request.Execute();
            Console.WriteLine("Upcoming events:");
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    string when = eventItem.Start.DateTime.ToString();
                    if (String.IsNullOrEmpty(when))
                    {
                        when = eventItem.Start.Date;
                    }
                    Console.WriteLine("{0} ({1}) {2}", eventItem.Summary, when, eventItem.Id);
                }
            }
            else
            {
                Console.WriteLine("No upcoming events found.");
            }
            DateTime dsa = DateTime.Parse("2015-11-11 11:11");
            Event e = new Event()
            {
                Summary = "Ignore",
                Location = "IGS " ,
                Start = new EventDateTime()
                {
                    DateTime = dsa.AddSeconds(60),
                    TimeZone = "Europe/London"
                },
                End = new EventDateTime()
                {
                    DateTime = dsa.AddSeconds( 30 * 60),
                    TimeZone = "Europe/London"
                },
                Description = "Part of Protocol "
            };
            String calendarId = "primary";
            EventsResource.InsertRequest request2 = service.Events.Insert(e, calendarId);
            try
            {
                Event createdEvent = request2.Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.Read();
        }
    }
}