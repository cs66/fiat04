-- Create a database called igsgs in instance SQLEXPRESS
-- Owner sysdba with pwd masterkey
use igsgs;
DROP TABLE targetenv;
DROP TABLE actions;
DROP TABLE attachment
DROP TABLE growth
DROP TABLE trial
DROP TABLE thco2

DROP TABLE segcol
DROP TABLE segment
DROP TABLE recipe
DROP TABLE session
DROP TABLE siteOwner
DROP TABLE site
DROP TABLE usr


CREATE TABLE usr
(id		VARCHAR(255) PRIMARY KEY
,passwd	NVARCHAR(255)
,admin	CHAR(1)
)
INSERT INTO usr(id,passwd,admin) VALUES ('sysdba',CONVERT(VARCHAR(32), HashBytes('MD5', 'masterkey'), 2),'y')
INSERT INTO usr(id,passwd,admin) VALUES ('andrew',CONVERT(VARCHAR(32), HashBytes('MD5', 'brazil'), 2),NULL)
INSERT INTO usr(id,passwd,admin) VALUES ('dave',CONVERT(VARCHAR(32), HashBytes('MD5', 'dave'), 2),NULL)

CREATE TABLE session
(token	VARCHAR(255) PRIMARY KEY
,usr	VARCHAR(255) FOREIGN KEY REFERENCES usr(id)
,IP		VARCHAR(50)
,settings TEXT
)

CREATE TABLE recipe
(rid INT IDENTITY(1,1) PRIMARY KEY
,rnm	NVARCHAR(255)					-- Recipe name
,ver	INT								-- Version
,usr	VARCHAR(255)					-- Created by usr
,whn	DATETIME						-- Creation time
,FOREIGN KEY (usr) REFERENCES usr(id)
)

CREATE TABLE segment
(rid	INT								-- Recipe id
,frm	INT								-- Start time in seconds
,utl	INT								-- Until in seconds
,prf	INT								-- Pulse rate frequence Hz
,prd	INT								-- Period - repeat after this long (seconds)
,FOREIGN KEY(rid) REFERENCES recipe(rid)
,PRIMARY KEY (rid,frm)
)

CREATE TABLE segcol
(rid	INT
,frm	INT
,col	NVARCHAR(15)					-- Colour channel (red, green, blue...)
,crr	INT								-- Current mAmps
,dty	INT								-- Duty cycle time (percentage)
,rmp	INT								-- Time to spent ramping from previous value to new value (seconds)
,FOREIGN KEY (rid,frm) REFERENCES segment(rid,frm)
,PRIMARY KEY (rid,frm,col)
)

--INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Auto Aubergine', 1,'andrew','2015-07-08 14:49');
INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Bionic Beetroot', 1,'andrew','2015-07-08 14:49');
INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Clockwork Orange', 1,'andrew','2015-07-08 14:49');
INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Dilithium Damson', 1,'andrew','2015-07-08 14:49');
INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Electro Eggplant', 1,'andrew','2015-07-08 14:49');

INSERT INTO segment VALUES (1,      0, 1000000,100,30)
INSERT INTO segment VALUES (1, 1000000, 2000000,100,30)
INSERT INTO segment VALUES (1, 2000000, 3000000,100,30)
INSERT INTO segcol VALUES (1, 0, 'red',   447, 100, 0)
INSERT INTO segcol VALUES (1, 0, 'green',  77, 100, 0)
INSERT INTO segcol VALUES (1, 0, 'blue',   70, 100, 0)

INSERT INTO segcol VALUES (1,1000000, 'red',   350, 100, 0)
INSERT INTO segcol VALUES (1,1000000, 'green',  47, 100, 0)
INSERT INTO segcol VALUES (1,1000000, 'blue',   55, 100, 0)

INSERT INTO segcol VALUES (1,2000000, 'red',   350, 100, 0)
INSERT INTO segcol VALUES (1,2000000, 'green', 150, 100, 0)
INSERT INTO segcol VALUES (1,2000000, 'blue',  150, 100, 0)

INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (2, 0, 100000, 100, 0)
INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (2, 100000, 200000, 100, 0)
INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (3, 0, 100000, 100, 0) 
INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (3, 100000, 200000, 100, 0)
INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (4, 0, 100000, 100, 0)
INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (4, 100000, 200000, 100, 0)
--INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (5, 0, 100000, 100, 0)
--INSERT [dbo].[segment] ([rid], [frm], [utl], [prf], [prd]) VALUES (5, 100000, 200000, 100, 0)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 0, N'blue', 250, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 0, N'green', 77, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 0, N'red', 447, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 100000, N'blue', 255, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 100000, N'green', 47, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (2, 100000, N'red', 350, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 0, N'blue', 0, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 0, N'green', 346, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 0, N'red', 450, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 100000, N'blue', 100, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 100000, N'green', 346, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (3, 100000, N'red', 450, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 0, N'blue', 450, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 0, N'green', 3, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 0, N'red', 350, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 100000, N'blue', 400, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 100000, N'green', 0, 100, NULL)
INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (4, 100000, N'red', 350, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 0, N'blue', 10, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 0, N'green', 450, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 0, N'red', 450, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 100000, N'blue', 0, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 100000, N'green', 450, 100, NULL)
--INSERT [dbo].[segcol] ([rid], [frm], [col], [crr], [dty], [rmp]) VALUES (5, 100000, N'red', 450, 100, NULL)

/*
				frm utl prd rc  rd  gc  gd  bc  bd  prf
Bionic Beetroot	0	10	30	447	100	77	100	70	100	1000	True
Bionic Beetroot	10	20	30	350	100	47	100	55	100	1000	True
Bionic Beetroot	20	30	30	350	100	150	100	150	100	1000	True
Clockwork Orange	0	10	20	500	100	350	100	0	100	1000	True
*/

CREATE TABLE site
(id NVARCHAR(255) PRIMARY KEY,
mrupdate DATETIME,
settings TEXT
);

INSERT INTO site (id, settings,mrupdate) VALUES (
  'Buckstone',
  '{"trays":[{"id":"T1-01","serial":"1","lights":["red","green","blue"],"recipe":"1","startAt":"2015-07-19 17:24"},{"id":"T1-02","serial":"2","lights":["red","green","blue"],"recipe":"1","startAt":"2015-07-19 17:25"}]}',
  '2015-07-19 17:29'
);
--Create 12 trays
UPDATE site SET settings=
'{"trays":[{"id":"T1-01","serial":"1","lights":["red","green","blue"],"recipe":"6","startAt":"2015-11-11 01:39"},{"id":"T1-02","serial":"2","lights":["red","green","blue"],"recipe":"1","startAt":"2015-07-19 17:25"}],"towers":{"T1":{"subscribers":["igs.buckstone@gmail.com"]}}}'
   WHERE id='Buckstone'

CREATE TABLE siteOwner(
  usr VARCHAR(255) FOREIGN KEY REFERENCES usr(id),
  site NVARCHAR(255) FOREIGN KEY REFERENCES site(id)
  PRIMARY KEY (usr,site)
);

INSERT INTO siteOwner(usr,site) VALUES ('andrew','Buckstone')
INSERT INTO siteOwner(usr,site) VALUES ('dave','Buckstone')

CREATE TABLE trial
(tid INT IDENTITY(9000,1) PRIMARY KEY
,start	DATETIME
,rid	INT FOREIGN KEY REFERENCES recipe(rid)
,tray   VARCHAR(10)
,site	NVARCHAR(255) FOREIGN KEY REFERENCES site(id) 
);


CREATE TABLE attachment
(aid INT IDENTITY(1,1) PRIMARY KEY
,tid INT FOREIGN KEY REFERENCES trial(tid)
,whn DATETIME
,typ VARCHAR(10)
,fnm VARCHAR(255)
);

CREATE TABLE growth
(aid INT IDENTITY(1,1) PRIMARY KEY
,tid INT FOREIGN KEY REFERENCES trial(tid)
,whn DATETIME
,wgt float
);

DELETE FROM segcol  WHERE rid IN (SELECT rid FROM recipe WHERE rnm='Scottish Summer')
DELETE FROM segment WHERE rid IN (SELECT rid FROM recipe WHERE rnm='Scottish Summer')
DELETE FROM recipe WHERE rnm='Scottish Summer';
INSERT INTO recipe(rnm,ver,usr,whn) VALUES ('Scottish Summer', 1,'andrew','2015-07-14 14:49');
DECLARE @nr INT;
SET @nr = @@IDENTITY;
DECLARE @frm INT;
DECLARE @i int = 0
WHILE @i<14*7 BEGIN
	SET @frm = (SELECT COALESCE(MAX(utl),0) FROM segment WHERE rid=@nr);
	INSERT INTO segment VALUES (@nr,  @frm, @frm+18*60*60,100,null) -- 18 hours of light
	INSERT INTO segcol VALUES (@nr, @frm, 'red',   350, 100, 0)
	INSERT INTO segcol VALUES (@nr, @frm, 'green', 350, 100, 0)
	INSERT INTO segcol VALUES (@nr, @frm, 'blue',  350, 100, 0)
	SET @frm = (SELECT COALESCE(MAX(utl),0) FROM segment WHERE rid=@nr); -- 6 hours of dark
	INSERT INTO segment VALUES (@nr,  @frm, @frm+6*60*60,100,null)
	INSERT INTO segcol VALUES (@nr, @frm, 'red',   35, 100, 0)
	INSERT INTO segcol VALUES (@nr, @frm, 'green', 35, 100, 0)
	INSERT INTO segcol VALUES (@nr, @frm, 'blue',  35, 100, 0)
	SET @i = @i + 1
END

--Fake history of trials
INSERT INTO trial(start,rid,tray,site) VALUES('2015-02-26 09:00',1,'T1-01','Buckstone')
INSERT INTO trial(start,rid,tray,site) VALUES('2015-03-05 09:00',2,'T1-02','Buckstone')
INSERT INTO trial(start,rid,tray,site) VALUES('2015-03-12 09:00',@nr,'T1-03','Buckstone')

CREATE TABLE thco2
(site NVARCHAR(255)
,fst INT DEFAULT 0
,lst INT DEFAULT 1000000
,whn DATETIME
,temp float
,co2 int
,humidity float
,PRIMARY KEY (site,fst,whn)
);

delete from segcol where rid in (SELECT rid from recipe where rnm='Stunned Spud');
delete from segment where rid in (SELECT rid from recipe where rnm='Stunned Spud');
delete from trial where rid in (SELECT rid from recipe where rnm='Stunned Spud');
delete from recipe where rnm = 'Stunned Spud';
go

drop table nums;
create table nums(i int primary key)
insert into nums values (0);
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums
insert into nums select (select count(1) FROM nums)+i FROM nums

insert into recipe(rnm,ver,usr,whn) values ('Stunned Spud',1,'andrew','2015-07-08');
declare @sts int = @@identity;

-- long day, short night
insert into segment
 select @sts,i*24*60*60,i*24*60*60+18*60*60,100,null 
   from nums
  where i<35;
--select * from segment
insert into segment
 select @sts,i*24*60*60+18*60*60,(i+1)*24*60*60,99,null 
   from nums
  where i<35;

insert into segcol
  select @sts,frm,'red',350,100,0
    from segment where rid=@sts and prf=100
insert into segcol
  select @sts,frm,'green',350,100,0
    from segment where rid=@sts and prf=100
insert into segcol
  select @sts,frm,'blue',350,100,0
    from segment where rid=@sts and prf=100
insert into segcol
  select @sts,frm,'red',35,100,0
    from segment where rid=@sts and prf=99
insert into segcol
  select @sts,frm,'green',35,100,0
    from segment where rid=@sts and prf=99
insert into segcol
  select @sts,frm,'blue',35,100,0
    from segment where rid=@sts and prf=99
-- stun the spud - no light between days 130 and 13
update segcol set crr = 35 where rid=@sts and frm between 10*24*60*60 and 13*24*60*60

INSERT INTO trial(start,rid,tray,site) VALUES('2015-02-23 09:00',@sts,'T1-04','Buckstone')
DECLARE @sstrial int = @@identity
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,2,'2015-02-23 09:00'),3.2)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,8,'2015-02-23 09:00'),4.2)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,10,'2015-02-23 09:00'),4.2)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,12,'2015-02-23 09:00'),4.0)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,15,'2015-02-23 09:00'),4.3)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,17,'2015-02-23 09:00'),4.4)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,20,'2015-02-23 09:00'),8.1)
INSERT INTO growth(tid,whn,wgt) values (@sstrial,dateadd(day,28,'2015-02-23 09:00'),10)

insert into attachment(tid,whn,typ,fnm) values (@sstrial,'2015-02-25','img','IMG_1412.JPG')
insert into attachment(tid,whn,typ,fnm) values (@sstrial,'2015-03-03','img','IMG_1414.JPG')
insert into attachment(tid,whn,typ,fnm) values (@sstrial,'2015-03-15','img','IMG_1413.JPG')

INSERT INTO trial(start,rid,tray,site) SELECT '2015-03-01 09:00',rid,'T1-01','Buckstone' from recipe where rnm = 'Auto Aubergine'

delete from thco2;
-- introduce three measurements per day (more will give a more consistent min and max)

insert into thco2(site,fst,lst,whn)
  select 'Buckstone', 0, 1000000, dateadd(day, i ,' 2015-02-25 08:00')
    from nums where i between 0 and 35
insert into thco2(site,fst,lst,whn)
  select 'Buckstone', 0, 1000000, dateadd(day, i ,' 2015-02-25 09:00')
    from nums where i between 0 and 35
insert into thco2(site,fst,lst,whn)
  select 'Buckstone', 0, 1000000, dateadd(day, i ,' 2015-02-25 10:00')
    from nums where i between 0 and 35
drop function wrapped_rand
drop view wrapped_rand_view
go
create view wrapped_rand_view
as
    select rand( ) as random_value
go
create function wrapped_rand()
returns float as
begin
    declare @f float
    set @f = (select random_value from wrapped_rand_view)
    return @f
end
go
update thco2
 set temp = round(10+5*dbo.wrapped_rand(),1)
 ,   co2 = round(450+150*dbo.wrapped_rand(),0)
 ,   humidity = round(97 - 45*dbo.wrapped_rand(),1)
-- Introduce a cold snap between days 10 and 13
update thco2 set temp = temp-5 where whn between dateadd(day,10,'2015-02-25') and dateadd(day,14,'2015-02-25')

drop table actions
create table actions(
rid INT,
frm INT,
act VARCHAR(10),
msg VARCHAR(100),
PRIMARY KEY (rid,frm,act),
FOREIGN KEY (rid) REFERENCES recipe(rid)
)
INSERT INTO actions SELECT 6,24*60*60*i+12*60*60,'W','Water the plants' FROM nums WHERE i<32
INSERT INTO actions SELECT 6,5*24*60*60*i+13*60*60,'C','Clean the trays' FROM nums WHERE i<5
INSERT INTO actions VALUES (6,24*60*60*34+12*60*60,'H','Harvest the crop')
INSERT INTO actions VALUES (6,24*60*60*17+12*60*60,'I','Inspection - check weight')
--{"trays":[{"id":"T1-01","serial":"1","lights":["red","green","blue"],"recipe":"1","startAt":"2015-11-29 18:04"}],"towers":{"T1":{"subscribers":["igs.buckstone@gmail.com"]}}}

CREATE TABLE targetenv(
rid INT,
frm INT,
prm VARCHAR(10),
tgt FLOAT,
PRIMARY KEY (rid,frm,prm),
FOREIGN KEY (rid) REFERENCES recipe(rid)
);
INSERT INTO targetenv VALUES (6,0,'co2',77.6);
INSERT INTO targetenv VALUES (6,0,'t',14.0);
INSERT INTO targetenv VALUES (6,9.5*24*60*60,'t',5.0)
INSERT INTO targetenv VALUES (6,13.5*24*60*60,'t',14.0)
INSERT INTO targetenv VALUES (6,0,'h',90.0);
INSERT INTO targetenv VALUES (6,9.5*24*60*60,'h',50.0)
INSERT INTO targetenv VALUES (6,13.5*24*60*60,'h',86.0)
