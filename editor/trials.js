﻿$(function () {
    $('#trial-from,#trial-to').blur(function () {
        $.getJSON('data.aspx', { action: 'get-trials', from: $('#trial-from').val(), to: $('#trial-to').val(), site: $('#site-id').text() }, function (d) {
            if (d.length===0)
                return;
            $('table#trials').remove();
            $('#svg-container').before($('<table/>', { id: 'trials' }).append($('<tr/>')
                .append($('<th/>', { text: 'trial id' }))
                .append($('<th/>', { text: 'tray' }))
                .append($('<th/>', { text: 'protocol' }))
                ));
            var lo = new Date(d[0][1]).getTime();
            var hi = lo + 1000 * d[0][5];
            var lst = [[lo, hi]];
            for (var i = 1; i < d.length; i++) {
                var st = new Date(d[i][1]).getTime();
                var en = st + d[i][5] * 1000;
                lst.push([st,en]);
                lo = Math.min(lo, st);
                hi = Math.max(hi, en);
            }
            var sf = 400/(hi-lo);
            for (var i = 0; i < d.length; i++) {
                $('#trials').append($('<tr/>')
                    .append($('<td/>').append($('<div/>', {
                        text: d[i][0], css: { color: 'blue', cursor: 'pointer' }, click: showTrial,
                        data: { details: d[i] },
                        'class':'tid'
                    })))
                    .append($('<td/>', { text: d[i][3] }))
                    .append($('<td/>', { text: d[i][4] }))
                    .append($('<td/>').append($('<div/>', {
                        css: {
                            'margin-left': (lst[i][0] - lo) * sf + 'px',height:'4px',
                            width: (lst[i][1] - lst[i][0]) * sf + 'px',
                            border: 'solid thin green','background-color':'rgb(128,225,128)'
                        },
                        title:(new Date(lst[i][0])) + " to "+(new Date(lst[i][1]))
                    })))
//                    .append($('<td/>').append($('<div/>', { 'class': 'drop-zone' })))
                );
            }
        });
    });
    //$('#trial-from').trigger('blur'); - this call is made on the return from the "who-am-i" call
});

function togglethco2() {
    var thco2 = $('#trial-svg').data('thco2');
    if (!thco2) {
        var chkbx = $(this);
        $.getJSON('data.aspx', { action: 'get-thco2', site: $('#site-id').text(), lo: '2015-01-01', hi: '2016-01-01' }, function (d) {
            var st = (new Date($('#trial-svg').data('details')[1])).getTime();
            var thco2 = {};
            for (var i = 0; i < d.length; i++) {
                var day = Math.floor(((new Date(d[i][1])).getTime() - st) / 24 / 60 / 60 / 1000);
                if (i == 0) $('#trial-svg').data('thco2-fst-day', Math.max(day,0));
                if (i == d.length-1) $('#trial-svg').data('thco2-lst-day', day);
                thco2['day' + day] = {  // Daily Min, Max, Average 
                    day: day, temperature: [d[i][3], d[i][4], d[i][5]], humidity: [d[i][6], d[i][7], d[i][8]],
                    co2: [d[i][9], d[i][10], d[i][11]]
                };
            }
            $('#trial-svg').data('thco2', thco2);
            chkbx.trigger('change');
            return;
        }).error(function(){
            $('#trial-svg').data('thco2', 'error');
        })
        return;
    }
    if (thco2 === "error") {
        alert("Did not load environmental values");
        return;
    }
    var svg = $('#trial-svg');
    var prop = $(this).attr('id');
    if ($(this).is(':checked')) {
        var hsh = svg.data('thco2');
        addDayCurveMMA(svg, prop, function (i) {
            if (hsh['day' + i])
                return hsh['day' + i][prop];
            else
                return;
        })
    } else {
        $('#area' + prop).remove();
    }
}

function showTrial() {
    var trialid = $(this).text();
    var details = $(this).data('details');
    $('.currentTrial').removeClass('currentTrial');
    $(this).parents('tr').addClass('currentTrial');
    var svg = $('#trial-svg');
    svg.attr('viewBox', "0 0 " + svg.width() + " 700");
    $('*',svg).not('style').remove(); //must note remove the style
    svg.data('details', details);
    $('#trial-controls').remove();
    var controls = $('<div/>', { id: 'trial-controls' })
        .append($('<span/>', { css: { 'margin-right': '3em' } }).append($('<input/>', { 'type': 'checkbox', id:'temperature' })).append(' Temperature (C)'))
        .append($('<span/>', { css: { 'margin-right': '3em' } }).append($('<input/>', { 'type': 'checkbox', id:'humidity' })).append(' Humidity (%)'))
        .append($('<span/>', { css: { 'margin-right': '3em' } }).append($('<input/>', { 'type': 'checkbox', id:'co2' })).append(' CO2 (ppm)'));
    svg.after(controls);
    $('#trial-controls input').change(togglethco2);
    var title = $SVG('text', {
        x: 10, y: 10,
        fill: 'black'
    }).append("Report on trial: " + details[0] + ", Tray: " + details[3] + ", Protocol: " + details[4]).appendTo(svg);
    $.getJSON('data.aspx', { action: 'get-recipe', rnm: details[4] }, function (d) {
        var top = d.segments[d.segments.length - 1][1];
        svg.data('duration', top);
        var cummul = [];
        var dailykwh = {};
        var day = 0;
        var kwh = 0;
        for (var i = 0; i < d.segments.length; i++) {
            var rate = 0;
            for (var j = 3; j < d.segments[i].length; j += 2)
                rate += d.segments[i][j] * d.segments[i][j + 1] / 100;
//            d.segments[i][0];
            while (day < d.segments[i][1] / 24 / 60 / 60) {
                dailykwh[day] = kwh;
                kwh += rate;
                day++;
            }
        }
        var w = svg.width();
        var h = svg.height();
        for (var i = 0; i <= day; i++) {
            svg.append($SVG('path', {stroke:'gray','stroke-width':1,d:'M'+Math.round(i*w/day)+' 0V'+h}));
        }
        addCurve(svg, 0, Math.ceil(top / 24 / 60 / 60), function (i) { return dailykwh[i]; });
        $.getJSON('data.aspx', { action: 'get-trial-mass', tid: trialid }, function (d) {
            var t0 = (new Date(details[1])).getTime();
            for (var i = 0; i < d.length; i++)
                d[i][0] = ((new Date(d[i][0])).getTime() - t0) / 24 / 60 / 60 / 1000;
            addMassCurve(svg, d);
        })
        $.getJSON('data.aspx', { action: 'get-trial-attachment', tid: trialid }, function (d) {
            var t0 = (new Date(details[1])).getTime();
            for (var i = 0; i < d.length; i++)
                d[i][0] = ((new Date(d[i][0])).getTime() - t0) / 24 / 60 / 60 / 1000;
            addAttachment(svg, d);
        })
    });
}

function addAttachment(svg, lst) {
    var lo = svg.data('lo');
    var hi = svg.data('hi');
    var w = svg.width();
    var h = svg.height();
    var x1;
    for (var i = 0; i < lst.length; i++) {
        var x0 = (lst[i][0] - lo) * w / (hi - lo);
        //<image xlink:href="firefox.jpg" x="0" y="0" height="50px" width="50px"/>
        var g = $SVG('g', { width: 400, height: 300, transform: 'translate('+x0+','+ (h - 50)+')','class':'hovGrp' });
        var link = $SVG('image', { height: 50, width: 50 });
        link[0].setAttributeNS('http://www.w3.org/1999/xlink', 'href', "attachment/camera-icon.png");
        var img = $SVG('image', { height: 00, width: 0, y:-300, 'class':'hide' });
        img[0].setAttributeNS('http://www.w3.org/1999/xlink', 'href', "attachment/" + lst[i][2]);
        g.append(img);
        g.append(link);
        svg.append(g);
    }
}

function addMassCurve(svg, lst) {
    var lo = svg.data('lo');
    var hi = svg.data('hi');
    var maxy = Number.NEGATIVE_INFINITY;
    for (var i = 0; i < lst.length; i++) {
        var v = lst[i][1];
        if (v > maxy) maxy = v;
    }
    var w = svg.width();
    var h = svg.height();
    var sy = h / maxy * .8;
    var x1;
    var y1;
    for (var i = 0; i < lst.length; i++) {
        var x0 = (lst[i][0] - lo) * w / (hi - lo);
        var y0 = h - lst[i][1] * sy;
        svg.append($SVG('circle', { cx: x0, cy: y0, r: 5, fill: 'blue' }));
        if (i > 0) {
            svg.append($SVG('line', {x2:x0,y2:y0,x1:x1,y1:y1,stroke:'blue','stroke-width':4,'stroke-dasharray':"5, 5"}))
        }
        x1 = x0;
        y1 = y0;
    }
    //    var path = "<path stroke='black' fill='none' d=\"M0 0L" + s.join("L") + "\"/>";
    //    console.log(path);
//    var d = 'M' + fst + "L" + s.join("L");
//    svg.data('lo', lo);
//    svg.data('hi', hi);
//    svg.append($SVG("path", { stroke: 'gray', 'stroke-width': 3, fill: 'none', d: d }));
}

function addDayCurveMMA(svg, prop, f) { // f returns [min,max,ave] for each day - lo and hi have been set by addCurve
    var fillcols = { temperature: 'rgba(255,0,0,.5)', humidity: 'rgba(128,255,128,.5)', co2: 'rgba(0,0,255,.3)' };
    var strocols = { temperature: 'rgb(128,0,0)', humidity: 'rgb(0,128,0)', co2: 'rgb(0,0,128)' };
    var fill = fillcols[prop];
    var stroke = strocols[prop];
    var lo = svg.data('lo');
    var hi = svg.data('hi');
    var r = [];
    var max = Number.NEGATIVE_INFINITY;
    var min = Number.POSITIVE_INFINITY;
    var fst = -1;
    var lst;
    for (var i = lo; i < hi; i++) {
        var v = f(i);
        if (v) {
            if (fst === -1) fst = i;
            lst = i;
            r.push({ v: v, day: i });
            if (v[1] > max) max = v[1];
            if (v[0] < min) min = v[0];
        }
    }
    //Keep the curve to 1/3 the height
    var sx = svg.width() / (hi - lo);
    var ymin = 0;
    var sy = svg.height() / max;
    var g = $SVG('g',{id:'area'+prop , transform:'translate(0,'+svg.height()+') scale(1,-1)'})
    for (var i = 0; i < r.length-1; i++) {
        var x0 = r[i].day * sx; var x3 = x0;
        var x1 = r[i + 1].day * sx; var x2 = x1;
        var y0 = (r[i].v[1] - ymin) * sy;
        var y1 = (r[i+1].v[1] - ymin) * sy;
        var y2 = (r[i+1].v[0] - ymin) * sy;
        var y3 = (r[i].v[0] - ymin) * sy;
        var quad = $SVG('path', {
            d: 'M' + x0 + ' ' + y0 + 'L' + x1 + ' ' + y1 + ' ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3 + 'z',
            stroke: 'none',
            fill: fill
        });
        g.append(quad);
        var y0a = (r[i].v[2] - ymin)*sy;
        var y1a = (r[i+1].v[2] - ymin) * sy;
        var line = $SVG('path', {
            d: 'M' + x0 + ' ' + y0a + 'L' + x1 + ' ' + y1a,
            stroke: stroke,
            'stroke-width':4,
            fill:'none'
        });
        g.append(line);
    }
    svg.append(g);
}

function addCurve(svg, lo, hi, f) {
    var maxy = Number.NEGATIVE_INFINITY;
    var r = [];
    for (var i = lo; i < hi; i++) {
        var v = f(i);
        r.push([i, v]);
        if (v > maxy) maxy = v;
    }
    var s = [];
    var w = svg.width();
    var h = svg.height();
    for (var i = 0; i < r.length; i++)
        s.push(Math.round(r[i][0] * w / (hi - lo)) + " " + (Math.round(h - r[i][1] * h / maxy)));
    var fst = s.shift();
//    var path = "<path stroke='black' fill='none' d=\"M0 0L" + s.join("L") + "\"/>";
    //    console.log(path);
    var d = 'M' + fst + "L" + s.join("L");
    svg.data('lo', lo);
    svg.data('hi', hi);
    svg.append($SVG("path", { stroke: 'gray','stroke-width':3, fill: 'none', d: d }));
}


//Deal with drag dropped images
$(function () {
    var holder = document.getElementById('svg-container');
    holder.ondragover = function () {
        $(this).addClass('hover');
        return false;
    };
    holder.ondragend = function () {
        $(this).removeClass('hover');
        return false;
    };
    holder.ondrop = function (e) {
        $(this).removeClass('hover');
        e.preventDefault();

        var tid = $('.currentTrial .tid').text();
        if (!tid) {
            alert('Please select a trial before adding attachments.');
            return;
        }
        var files = e.dataTransfer.files;
        var formData = new FormData();
        for (var i = 0; i < files.length; i++) {
            formData.append('file', files[i]);
        }
        formData.append('tid', tid);
        // now post a new XHR request
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/upload.aspx');
        xhr.onload = function () {
            if (xhr.status === 200) {
                console.log('all done: ' + xhr.status);
                console.log(xhr.response);
                $('.currentTrial .tid').trigger('click');
            } else {
                console.log('Something went terribly wrong...');
            }
        };

        xhr.send(formData);
    };


})
