﻿// Each color has a unique three letter acronym (tla)
// Each color has a unique one letter acronym (ola)
// The order of this table matters
// You can add new colors must you must never change one
var ys = 300 / 1500;
var bdr = 4;
var knownColors = [
    { longName: "red", tla: 'red', ola: 'r', wl: 660, show: "rgba(255,0,0,0.4)" },
    { longName: "green", tla: 'grn', ola: 'g', wl: 530, show: "rgba(0,255,0,0.4)" },
    { longName: "blue", tla: 'blu', ola: 'b', wl: 455, show: "rgba(0,0,255,0.4)" },
    { longName: "violet-1", tla: 'v1', ola: 'v1', wl: 422, show: "rgba(128,0,255,0.4)" },
    { longName: "violet-2", tla: 'v2', ola: 'v2', wl: 420, show: "rgba(255,0,255,0.4)" },
    { longName: "hyper-red", tla: 'hr', ola: 'hr', wl: 730, show: "rgba(255,0,0,0.6)" },
];
/*
Colour	Wavelength	Part number	Manufacturer	Qty
Hyper Red	660nm	LHCPDP	Osram	4200
Green	530nm	LTCPDP	Osram	1800
Blue	455nm	LDCQDP	Osram	1757
Red	730nm	GFCSHPM1	Osram	200
Violet 1	422nm	C3535U	    SemiLed	1000
Violet 2	420nm	C3SL-U-A	SemiLed	1000

*/
//Target values colours
var prms = [{ prm: 't', text: 'temperature', color: 'rgba(236,176,40,.5)' },
            { prm: 'h', text: 'humidity', color: 'rgba(58,95,11,.5)' },
            { prm: 'co2', text: 'CO2', color: 'rgba(192,192,192,.5)' }];
var knownColorsH = {};
var knownColorsO = {};
for (var i = 0; i < knownColors.length; i++) {
    knownColorsH[knownColors[i].longName.toLowerCase()] = knownColors[i];
    knownColorsO[knownColors[i].ola.toLowerCase()] = knownColors[i];
}
var openRecipeDialog =
    $("<div/>", { 'class': 'dialog', title: 'Open Recipe', id: "open-dialog" })
            .append($("<p/>", { text: "Enter recipe name: " })
                .append($("<input/>", { id: 'input-recipe' })
                    .autocomplete({
                        source: function (request, response) {
                            request['action'] = 'get-recipe-list';
                            $.getJSON('data.aspx', request, response).fail(response);
                        },
                        open: function (event, ui) {
                            var dialog = $(this).closest('.ui-dialog');
                            if (dialog.length > 0) {
                                $('.ui-autocomplete.ui-front').zIndex(dialog.zIndex() + 1);
                            }
                        }
                    }))
                .append($('<button/>', {
                    text: 'Open', click: function () {
                        loadRecipe($('#input-recipe').val());
                        $('#open-dialog').dialog("close");
                    }
                })))

$(function () {
    $('#tabs').tabs({ active: 0 });
    $.getJSON("data.aspx", { action: "who-am-i" }, function (d) {
        if (d.id) {
            $('#usr-name').text(d.id);
            $('#login').hide();
            $('#logout').show();
            if (d.settings) {
                if (d.settings['recipe-name'])
                    loadRecipe(d.settings['recipe-name'], true);
                if (d['sites']) {
                    if (d['sites'].length == 1)
                        loadSite(d['sites'][0]);
                    if (d.settings['site-name'])
                        loadSite(d.settings['site-name']); //TODO - this logic is not entirely right
                }
            }
        } else {
            $('#login').show();
            $('#logout').hide();
        }
    });

    $('#button-login').click(function () {
        $.getJSON('data.aspx', { usr: $('#usr').val(), passwd: $('#passwd').val(), action: 'login' },
            function (d) {
                if (d.id)
                    $('#usr-name').text(d.id);
                else
                    alert("User name or password incorrect.");
            })
        .fail(function () {
            alert("That didn't go so well.");
        });
    });

    $('#button-logout').click(function () {
        $.getJSON('data.aspx', { action: 'logout' },
            function (d) {
                $('#usr-name').text('none');
            });
    });
    $('#button-nuke-cal').click(function () {
        $.getJSON('data.aspx', {action:'nuke-cal'},
            function (d) { })
    });
    // Save and save as functions
    var saveWithNoFurtherAdo = function (rnm, d) {
        $.ajax({
            url: 'data.aspx', data: {
                action: 'save-recipe',
                rnm: rnm,
                cols: d.cols.join(","),
                segs: d.segments.join("\n")
            },
            method:'post',
            success: function (d) {
                if (d.err) {
                    alert(d.err);
                    return;
                }
                $('#recipe-name').text(rnm);
                sendSettings();
            },
            fail: function () { alert("save recipe failed"); }
        });
    }
    $('#saveas-dialog').dialog({
        autoOpen: false,
        buttons: [{
            text: "Save",
            click: function () {
                var d = $('body').data('current-recipe');
                var rnm = $('#saveas-dialog input').val();
                if (!rnm) {
                    alert("Bad name for a recipe");
                    $('#saveas-dialog button').dialog('close');
                    return;
                }
                $.ajax({
                    url: 'data.aspx', data: { action: 'get-recipe-list', term: rnm }, success: function (rlist) {

                        if ($.grep(rlist, function (n, i) { return n.label.toLowerCase() == rnm.toLowerCase() }).length == 0) {
                            saveWithNoFurtherAdo(rnm, d);
                            $('#saveas-dialog').dialog('close');
                        }
                        else {
                            $('#saveas-are-you-sure-dialog .yes').click(function () {
                                saveWithNoFurtherAdo(rnm, d);
                                $('#saveas-dialog').dialog('close');
                                $('#saveas-are-you-sure-dialog').dialog('close');
                            });
                            $('#saveas-are-you-sure-dialog .no').click(function () {
                                $('#saveas-are-you-sure-dialog').dialog('close');
                            });
                            $('#saveas-are-you-sure-dialog').dialog('open');
                        }
                    }
                });
            }
        }]
    });
    $('#saveas-dialog input').on('keydown', function (e) {
        if (e.keyCode == 13)
            $('button', $(this).parents('.ui-dialog'))
                .filter(function () { return $(this).text() == "Save" })
                .trigger('click');
    });
    $('#saveas-are-you-sure-dialog').dialog({ autoOpen: false });
    $('#button-save').click(function () {
        var rnm = $('#recipe-name').text();
        if (rnm != 'none')
            if ($('body').data('current-recipe'))
                saveWithNoFurtherAdo(rnm, $('body').data('current-recipe'));
            else
                alert("No recipe to save");
        else
            alert("No name specified");
    });
    $('#button-saveas').click(function () {
        var d = $('body').data('current-recipe');
        if (!d) {
            alert("Nothing to save");
            return;
        }
        $('#saveas-dialog').dialog('open');
    });
    $('#button-open').click(function () {
        openRecipeDialog.dialog();
    });
    $('#button-open').button({ "icons": { primary: "ui-icon-folder-open" } });
    $('#button-new').button({ "icons": { primary: "ui-icon-document" } });
    $('#button-save').button({ "icons": { primary: "ui-icon-disk" } });
    $('#button-saveas').button({ "icons": { primary: "ui-icon-disk" } });
    $('#button-parse').click(function () {
        /* Parse text input such as:
int	prf	rc	rd	gc	gd	bc	bd
10	100	447	100	77	100	70	100
10	100	350	100	47	100	55	100
10	100	350	100	150	100	150	100
        */
        var lns = $('#raw-text').val().split('\n');
        var d = { cols: [], rnm: "new", segments: [] };
        var dataLines = [];
        var headerLine = [];
        var columnH = {};
        var permitted = { int: 1, rmp: 1, prf: 1, ru: 1, rd:1 };
        for (var i = 0; i < knownColors.length; i++) {
            permitted[knownColors[i].ola + '.c'] = { longName: knownColors[i].longName, offset: 0 };
            permitted[knownColors[i].ola + '.d'] = { longName: knownColors[i].longName, offset: 1 };
        }
        var maxCol = 0;
        var lineStarts = [0];
        var acc = 0;
        for (var i = 0; i < lns.length; i++) {
            acc += lns[i].length + 1;
            lineStarts.push(acc);
        }
        for (var i = 0; i < lns.length; i++) {
            if (lns[i].length < 1) continue;
            if (lns[i][0] == '#') continue;
            var wrds = lns[i].split(/\t|,/);
            if (wrds.length < 3) continue;
            if (wrds[0].length < 1) continue;
            //First line must be the header
            headerLine = wrds;
            for (var j = 0; j < wrds.length; j++) {
                if (permitted[wrds[j]]) {
                    columnH[wrds[j]] = j;
                    continue;
                }
                console.log("Unknown column in header row line " + (i + 1) + ": " + wrds[j]);
            }
            maxCol = wrds.length;
            firstDataLine = i + 1;
            break;
        }
        var colList = ['frm', 'utl', 'prf'];
        for (var j = 0; j < knownColors.length; j++) {
            if (columnH[knownColors[j].ola + '.c'] || columnH[knownColors[j].ola + '.d']) {
                d.cols.push(knownColors[j].longName.toLowerCase());
                colList.push(knownColors[j].ola + '.c');
                colList.push(knownColors[j].ola + '.d');
            }
        }
        var start = 0;
        var charPosn = [];
        for (var i = firstDataLine; i < lns.length; i++) {
            wrds = lns[i].split(/\t|,/);
            if (wrds.length >= colList.length - 1) {
                var utl = start + parseInt(wrds[columnH['int']]);
                var seg = [start, utl];
                start = utl;
                for (var j = 2; j < colList.length; j++)
                    if (columnH[colList[j]])
                        seg.push(parseInt(wrds[parseInt(columnH[colList[j]])]));
                    else
                        seg.push(0);
                charPosn.push(lineStarts[i]);
                d.segments.push(seg);
            }
        }
        $('#raw-text').data('charPosn', charPosn);
        drawChartHTML(d);
        $('#drop-zone-lights').empty().append(mkTableLights(d));
        $('body').data('current-recipe', d);
    });
});

function mkTableTarget(prm, d) {
    var ret = $('<table/>');
    var hdr = $('<tr/>')
        .append($('<th/>', { text: 'frm' }))
        .append($('<th/>', { text: 'tgt', css: { 'background-color': prms.filter(function (x) { return x.prm==prm })[0].color} }));
    ret.append(hdr);
    for (var i = 0; i < d.targets.length; i++) {
        if (d.targets[i][1] == prm) {
            var tr = $('<tr/>', { id: 'tgt_' + i })
                .append($('<td/>', { 'class': 'long-num' })
                    .append($('<input/>', { value: secondsToDHM(d.targets[i][0]), spellcheck:false })))
                .append($('<td/>', { 'class': 'short-num' })
                    .append($('<input/>', { value: d.targets[i][2], spellcheck: false }))
                    )
                .appendTo(ret);
        }
    }
    return ret;
}

function mkTableActions(d) {
    var ret = $('<table/>');
    var hdr = $('<tr/>')
        .append($('<th/>', { text: 'frm' }))
        .append($('<th/>', { text: 'act' }))
        .append($('<th/>', { text: 'msg' }));
    ret.append(hdr);
    var classes = ['long-num','short-text','long-text'];
    for (var i = 0; i < d.actions.length; i++) {
        var tr = $('<tr/>', {id:'atr_'+i});
        for (var j = 0; j < d.actions[i].length;j++)
            tr.append($('<td/>', { 'class': classes[j] }).append(
                $('<input/>', {
                    value: (j == 0) ? secondsToDHM(d.actions[i][j]) : d.actions[i][j],
                    id: 'action_' + i + '_' + j,
                    data: { i: i, j: j },
                    focus: function () {
                        $('.selected').removeClass('selected');
                        $('#at_' + $(this).data('i')).addClass('selected');
                    },
                    keyup: function () {
                        var j = $(this).data('j');
                        if (j == 1) {//short code - a single letter
                            $('#at_' + $(this).data('i')).text($(this).val().toLowerCase());
                        }
                        if (j == 2) {//msg
                            $('#at_' + $(this).data('i')).attr('title',$(this).val().toLowerCase());
                        }
                    }
                })));
        tr.append($('<td/>')
            .append($('<button/>', {
                text: '+', click: function () {

                }
            }))
            .append(' ')
            .append($('<button/>', {
                data:{i:i},
                text: '-', click: function () {
                    $('#at_' + $(this).data('i')).remove();
                    $('#atr_' + $(this).data('i')).remove();
                }
            })));
        ret.append(tr);
    }
    return ret;
}
function lz(n) {
    if (n < 10) return '0' + n;
    else return n;
}
function secondsToDHM(n) {
    var s = n % 60;
    n = Math.floor(n / 60);
    var m = n % 60;
    n = Math.floor(n / 60);
    var h = n % 24;
    var d = Math.floor(n / 24);
    return d + 'd' + lz(h) + ":" + lz(m);
}
function mkTableLights(d) {
    var ret = $('<table/>');
    var hdr = $('<tr/>')
        .append($('<th/>', { text: 'int' }))
        .append($('<th/>', { text: 'prf' }));
    for (var i = 0; i < d.cols.length; i++) {
        hdr.append($('<th/>', { text: d.cols[i] + '.c' }));
        hdr.append($('<th/>', { text: d.cols[i] + '.d' }));
    }
    ret.append(hdr);
    for (var i = 0; i < d.segments.length; i++) {
        var tr = $('<tr/>')
            .append($('<td/>', {'class':'long-num'})
                .append($('<input/>', { val: d.segments[i][1] - d.segments[i][0], id: 'cf_' + i  })))
            .append($('<td/>', { 'class':'short-num'  })
                .append($('<input/>', { val: d.segments[i][2], id: 'cp_' + i })))
        for (var j = 0; j < d.cols.length; j++) {
            tr.append($('<td/>', { 'class': 'short-num' }).append($('<input/>', { val: d.segments[i][3 + 2 * j], id: 'cc_' + i + '_' + j })))
              .append($('<td/>', { 'class': 'short-num' }).append($('<input/>', { val: d.segments[i][4 + 2 * j], id: 'cd_' + i + '_' + j })));
        }
        ret.append(tr);
    }
    $('input', ret)
        .on('focus', function () {
            var d = $(this).attr('id');
            var bits = d.split('_');
            $('.current-cell').removeClass('current-cell');
            var e = '.col-' + bits[1] + '.row-' + bits[2];
            console.log(e);
            $(e).addClass('current-cell');
        }).on('change', function () {
            var d = $(this).attr('id');
            var bits = d.split('_');
            var i = parseInt(bits[1]);
            var d = $('body').data('current-recipe')
            if (bits.length < 3) {
                //This is int or prf
                recalculateSegments(d);
                drawChartHTML(d);
                $('body').data('current-recipe', d);
                return;
            }
            var j = parseInt(bits[2]);
            d.segments[i][((bits[0] === 'cc') ? 0 : 1) + 3 + 2 * j] = parseInt($(this).val());
            var ybase = 0;
            for (var k = 0; k < d.cols.length; k++){
                var e = '.col-' + i + '.row-' + k;
                var h = ys * d.segments[i][3 + 2 * k] * d.segments[i][4 + 2 * k] / 100;
                $(e).animate({ height: h - bdr, 'margin-top': -ybase - h }, { duration: 500 });//todo
                ybase += h;
            }
        });
    return ret;
}
function recalculateSegments(d) {
    var trs = $('#drop-zone-lights table tr');
    var segments = [];
    var frm = 0;
    for (var i = 1; i < trs.length; i++) {
        var tds = $('input',$(trs[i]));
        var utl = frm + parseInt($(tds[0]).val());
        var seg = [frm, utl];
        frm = utl;
        for(var j=0;j<tds.length;j++)
            seg.push(parseInt($(tds[j]).val()))
        segments.push(seg);
    }
    d.segments = segments;
}
function sendSettings() {
    var settings = {
        "recipe-name": $('#recipe-name').text(),
        "tab": $('li.ui-state-active').attr('aria-controls')
    };
    $.getJSON("data.aspx", { action: 'update-settings', settings: JSON.stringify(settings) });
}

function isUnique(x) {
    var u = {};
    for (var i = 0; i < x.length; i++) {
        if (u[x[i]]) return false;
        u[x[i]] = 1;
    }
    return true;
};
(function ($) {
    //mkEdit - make this table cell editable, {text:'default text', validate:function(v){return true; action:function(){}}}
    $.fn.mkEdit = function(opts){
        $(this).append(
            $('<div/>', { css: { position: 'relative' }, 'class':'mkEdit' , data:{oldVal:opts.text}})
                .append($('<input/>', {
                    val: opts.text, 'class': 'editty',
                    css:{'margin-right':'3ex', width:'3ex'},
                    blur: function () {
                        var oldVal = $(this).parent().data('oldVal');
                        var newVal = $(this).val();
                        if (oldVal != newVal) {
                            var errMsg;
                            if (opts.validate && !opts.validate(newVal))
                                $(this).val(oldVal);
                            else {
                                $(this).parent().data('oldVal', newVal);
                                if (opts.action)
                                    opts.action();
                            }
                        }
                    }
                }))
            );
        return this;
    }
})(jQuery);
function mkTrayTable(stg) {
    var trayTable = $('<table/>', { id: 'trayTable' })
            .append($('<caption>Trays</caption>'))
            .append($('<tr/>')
                .append($('<th/>', { text: 'Tray' }))
                .append($('<th/>', { text: 'Serial Addr' }))
                .append($('<th/>', { text: 'Recipe' }))
                .append($('<th/>', { text: 'Start' }))
                .append($('<th/>', { text: 'Finish' }))
                .append($('<th/>', { text: 'Colours' }))
                //.append($('<th/>', { text: ' ' }))
            );
    for (var trayid in stg.trays) {
        var tray = stg.trays[trayid];
        var norecipe = tray.recipe === undefined;
        var tr = $('<tr/>', { id: 'tray_' + tray.id, data: { rid: tray.recipe } })
            .append($('<td/>', { text: tray.id, 'class': 'tray-id' }))
            .append($('<td/>', { 'class': 'serial' }).mkEdit({
                text: tray.serial,
                validate: function (s) {  // Must be numeric
                    return s.search(/^\d+$/) >= 0 &&
                        isUnique($('#trayTable td.serial input').map(function () { return $(this).val() }).toArray());
                },
                action: sendSiteSettings
            }))
            .append($('<td/>', {
                text: norecipe ? 'No protocol' : 'looking up protocol ' + tray.recipe + '...',
                'class': 'recipe-name'
            })) // Filling in this box continues at "Recipe td gets completed"
            .append($('<td/>', { text: tray.startAt, 'class': 'startAt' }))
            .append($('<td/>', { text: norecipe ? '' : 'Calculating...', 'class': 'finishAt' }))
            .append($('<td/>', { 'class': 'cols' })
                .append($('<span/>', { 'class': 'cols', text: tray.lights.join(", ") }))
                .append($('<button/>', {
                    'class': 'save',
                    title: 'Update database with these colours',
                    click: function () {
                        var td = $(this).parents('td');
                        var cols = $('input', td).val().split(/,\s*/);
                        var ok = true;
                        for (var i = 0; i < cols.length; i++) {
                            if (!knownColorsH[cols[i]])
                                ok = false;
                        }
                        if (ok) {
                            $('span.cols', td).text(cols.join(', '));
                            $('span.cols', td).show();
                            $('input', td).hide();
                            $(this).hide();
                            $('button.edit', td).show().css('display', 'inline');
                        } else {
                            alert('wrong colours');
                            $('span.cols', td).show();
                            $('input', td).hide();
                            $(this).hide();
                            $('button.edit', td).show().css('display', 'inline');
                        }
                    }
                }).hide())
                .append($('<button/>', {
                    'class': 'edit',
                    title: 'Change the colours on this tray', click: function () {
                        var td = $(this).parents('td');
                        $('span.cols', td).hide();
                        $('input', td).remove();
                        var inbox = $('<input/>').val($('span.cols', td).text());
                        $(td).prepend(inbox);
                        inbox.focus();
                        $(this).hide();
                        $('button.save', td).show().css('display', 'inline');
                    }
                }))
                )
//                .append(mkAdder())
            .appendTo(trayTable);
    }
    return trayTable;
}

function completeTrayTable(stg) {
    //Calculate end times and so forth
    $('button.save').button({ icons: { primary: 'ui-icon-disk' }, text: false });
    $('button.edit').button({ icons: { primary: 'ui-icon-pencil' }, text: false });
    $('button.del').button({
        icons: { primary: 'ui-icon-trash' }, text: false
    }).click(function () {

    });
    var recipeHash = {};
    for (var trayid in stg.trays) {
        var rid = stg.trays[trayid].recipe;
        if (rid)
            $.getJSON('data.aspx', { action: 'get-recipe', rid: rid }, function (d) {
                recipeHash[d.rid] = d;
                fillInTrayRows(rid, d);
            })
        else
            fillInTrayRows(rid);
    }
    $('#trayTable').data('recipeHash', recipeHash);
}
function loadSite(siteName) {
    $('#site-id').text('loading '+siteName  +"...")
    $.getJSON('data.aspx', { action: 'get-site-settings', site: siteName }, function (d) {
        var stg = d.settings;
        $('#site-id').text(siteName);
        $('#trial-from').trigger('blur'); // Get the trials data loaded - this is the earliest we can do it
        $('#trayTable').remove();
        var trayTable = mkTrayTable(stg);
        trayTable.appendTo('#tabs-3');
        completeTrayTable(stg);
        $('#towers-detail').remove();
        $('<div/>', { id: 'towers-detail', text: 'Towers: ' }).append($('<input/>', { id: 'towers', value: JSON.stringify(stg.towers) }))
            .hide()
            .appendTo('#tabs-3');
        mkTableTowerDetails(stg.towers).appendTo('#tabs-3');

    });
}

function fillInTrayRows(rid,d) {
    //Complete the rows relating to rid in the recipe table
    var totTime = (d && d.segments.length > 0) ? d.segments[d.segments.length - 1][1] : 0;
    $('tr', trayTable).each(function () { // Recipe td gets completed
        if ((d===undefined && $(this).data('rid')==="undefined") ||
            $(this).data('rid') == (d && d.rid)) {
            $('td.recipe-name', this)
                .empty()
                .append($('<span/>', { text: d?d.rnm:'', 'class': 'rnm' }))
                .append($('<button/>', {
                    title: 'Start a recipe', 'class': 'edit', click: function () {
                        var td = $(this).parents('td');
                        var nm = $('span', td).text();
                        $('span.rnm', td).hide();
                        var inBox = $('<input/>', { 'class': 'rnm' });
                        inBox.val(nm).prependTo(td);
                        inBox.autocomplete({
                            source: function (request, response) {
                                request['action'] = 'get-recipe-list';
                                $.getJSON('data.aspx', request, response).fail(response);
                            }
                        });
                        inBox.focus();
                        $(this).hide();
                        $('button.save', td).show().css('display', 'inline');
                    }
                }).button({ icons: { primary: 'ui-icon-pencil' }, text: false }))
                .append($('<button/>', {
                    'class': 'save', click: function () {
                        var rnm = $('input.rnm', td).val();
                        var td = $(this).parents('td');
                        if (rnm == "") {
                            $('span.rnm', td).text("");
                            $('input.rnm', td).hide();
                            $('button.save', td).hide();
                            $('button.edit', td).show().css('display', 'inline');
                            var tr = td.parents('tr');
                            tr.removeData('rid');
                            $('td.startAt', tr).empty();
                            $('td.finishAt', tr).empty();
                            sendSiteSettings(tr.attr('id').replace("tray_", ""));
                        } else {
                            $.getJSON('data.aspx', { action: 'get-recipe', rnm: rnm }, function (d) {
                                if (d.segments) {
                                    $('span.rnm', td).text($('input.rnm', td).val()).show();
                                    $('input.rnm', td).hide();
                                    $('button.save', td).hide();
                                    $('button.edit', td).show().css('display', 'inline');
                                    var tr = td.parents('tr');
                                    tr.data('rid', d.rid);
                                    $('td.finishAt', tr).empty();
                                    $('td.startAt', tr).empty();
                                    $('td.startAt', tr).append($('<button/>', {
                                        data: { recipe: d },
                                        text: 'Start Now', click: function () {
                                            var segs = $(this).data('recipe').segments;
                                            $(this).hide();
                                            var td = $(this).parents('td');
                                            var tr = $(this).parents('tr');
                                            tr.data('rid', d.rid);
                                            td.text(formatDateTime(new Date()));
                                            var totTime = segs[segs.length - 1][1];
                                            var endAt = (new Date(td.text().replace(' ', 'T'))).getTime() + totTime * 1000;
                                            $('td.finishAt', tr).text(formatDateTime(new Date(endAt)));
                                            sendSiteSettings(tr.attr('id').replace("tray_", ""));
                                        }
                                    }));
                                    $('td.startAt', tr).append($('<button/>', {
                                        data: { recipe: d },
                                        text: 'Start at...', click: function () {
                                            var segs = $(this).data('recipe').segments;
                                            $(this).hide();
                                            var td = $(this).parents('td');
                                            var tr = $(this).parents('tr');
                                            tr.data('rid', d.rid);
                                            td.empty();
                                            var daytime = formatDateTime(new Date()).split(' ');
                                            var f = function () {
                                                var dt = $('input', $(this).parent());
                                                var dtf = $(dt[0]).val() + "T" + $(dt[1]).val();
                                                var totTime = segs[segs.length - 1][1];
                                                var endAt = (new Date(dtf)).getTime() + totTime * 1000;
                                                $('td.finishAt', tr).text(formatDateTime(new Date(endAt)));
                                            };
                                            td.append($('<input/>', { value: daytime[0], css: { width: '12ex' }, blur: f }));
                                            td.append($('<input/>', { value: daytime[1], css: { width: '7ex' }, blur: f }));
                                            //sendSiteSettings();
                                        }
                                    }));
                                    $('td.finishAt', tr).append($('<button/>', {
                                        data: { recipe: d },
                                        text: 'End at...', click: function () {
                                            var segs = $(this).data('recipe').segments;
                                            $(this).hide();
                                            var td = $(this).parents('td');
                                            var tr = $(this).parents('tr');
                                            tr.data('rid', d.rid);
                                            td.empty();
                                            var daytime = formatDateTime(new Date()).split(' ');
                                            var f = function () {
                                                var dt = $('input', $(this).parent());
                                                var dtf = $(dt[0]).val() + "T" + $(dt[1]).val();
                                                var totTime = segs[segs.length - 1][1];
                                                var sttAt = (new Date(dtf)).getTime() - totTime * 1000;
                                                $('td.startAt', tr).text(formatDateTime(new Date(sttAt)));
                                            };
                                            td.append($('<input/>', { value: daytime[0], css: { width: '12ex' }, blur: f }));
                                            td.append($('<input/>', { value: daytime[1], css: { width: '7ex' }, blur: f }));
                                            //sendSiteSettings();
                                        }
                                    }));

                                } else {
                                    alert(d.err);
                                }
                            })
                        }
                    }
                }).hide().button({ icons: { primary: 'ui-icon-disk' }, text: false }));
            var endAt = new Date($('td.startAt', this).text().replace(' ', 'T')).getTime() + totTime * 1000;;
            if (! isNaN(endAt)) {
                $('td.finishAt', this).text(formatDateTime(new Date(endAt)));
            }
        }
    });
}
function mkPlus(tr) {
    return $('<div/>', {
        text: '+', 'class': 'plus', click: function () {
            tr.after($('<tr/>').append($('<td/>',{text:'new'})));
        }
    });
}
function mkAdder() {
    return $('<td/>', { style:'padding:0;position:relative' })
                .append($('<div/>', { 'class': 'adder_t', html: '&nbsp;', })
                    .mouseenter(function () {
                        $(this).append(mkPlus($(this).parents('tr').prev()));
                    })
                    .mouseleave(function () {
                        $(this).empty();
                    }))
                .append($('<div/>', { 'class': 'adder_m', html: '&nbsp;' }))
                .append($('<div/>', { 'class': 'adder_b', html: '&nbsp;' })
                    .mouseenter(function () {
                        $(this).append(mkPlus($(this).parents('tr')));
                    })
                    .mouseleave(function () {
                        $(this).empty();
                    })
                );
}
function mkTableTowerDetails(twrd) {
    var t = $('<table/>', {'class':'twrsub'})
        .append($('<tr/>')
            .append($('<th/>', { text: 'Tower' }))
            .append($('<th/>', { text: 'Subcribers' }))
            .append($('<th/>', { text: 'Controls' }))
            );
    var twrlst = [];
    for (var k in twrd) twrlst.push(k);
    twrlst.sort();
    for (var i = 0; i < twrlst.length; i++) {
        t.append($('<tr/>')
            .append($('<td/>', { text: twrlst[i] }))
            .append($('<td/>')
                .append($('<input/>', {
                    'class':'twrsubemail',
                    id: 'subscr_' + twrlst[i],
                    value: twrd[twrlst[i]].subscribers.join(', ')
                })))
            .append($('<td/>')
                .append($('<button/>', {
                    data : { tower: twrlst[i]},
                    text: 'Add Tray', click: function () {
                        var settings = getSettingsFromPage();
                        var lastTray = settings.trays[settings.trays.length - 1];
                        var bts = lastTray.id.split("-");
                        settings.trays.push({
                            id: bts[0] + "-" + zeroPad(parseInt(bts[1]) + 1, 2), lights: lastTray.lights,
                            serial:""+(1+parseInt(lastTray.serial))
                        });
                        $.post("data.aspx", {
                            action: 'update-site',
                            site: $('#site-id').text(), settings: JSON.stringify(settings),
                            addr: $(this).data('tower')
                        }, function () {
                            $('#trayTable').remove();
                            var trayTable = mkTrayTable(settings);
                            $('#towers-detail').before(trayTable);
                            completeTrayTable(settings);
                        });

                    }
                }))
                .append($('<button/>', {
                    data: { tower: twrlst[i] },
                    text: 'Delete Tray', click: function () {
                        var settings = getSettingsFromPage();
                        settings.trays.pop();
                        $.post("data.aspx", {
                            action: 'update-site',
                            site: $('#site-id').text(), settings: JSON.stringify(settings),
                            addr: $(this).data('tower')
                        }, function () {
                            $('#trayTable').remove();
                            var trayTable = mkTrayTable(settings);
                            $('#towers-detail').before(trayTable);
                            completeTrayTable(settings);
                        });
                    }
                }))
                ))
    }
    return t;
}

function getSettingsFromPage() {
    var settings = { trays: [], towers: JSON.parse($('#towers').val()) };
    var trs = $('#trayTable tr');
    for (var i = 0; i < trs.length; i++) {
        if ($('td.tray-id', trs[i]).length > 0) {
            var tray = {
                id: $('td.tray-id', trs[i]).text(),
                serial: $('td.serial input', trs[i]).val(),
                lights: $('td.cols span.cols', trs[i]).text().split(/,\s*/),
            };
            if ($(trs[i]).data('rid')){
                tray.recipe = $(trs[i]).data('rid') + "";  // Make it a string
                if ($('td.startAt', trs[i]).text())
                    tray.startAt = $('td.startAt', trs[i]).text();
            }
            settings.trays.push(tray);
        }
    }
    return settings;
}

function sendSiteSettings(addr) {
    var settings = getSettingsFromPage();
    $.post("data.aspx", { action: 'update-site', site: $('#site-id').text(), settings: JSON.stringify(settings),addr:addr });
}

function loadRecipe(rnm, donotupdatesetting) {
    $.getJSON('data.aspx', { action: 'get-recipe', rnm: rnm }, function (d) {
        $('#recipe-name').text(rnm);
        $('#span-recipe-name').text(rnm);
        $('#span-rid').text(d.rid);
        $('body').data('current-recipe', d);
        drawChartHTML(d);
        fillTextarea(d);
        $('#drop-zone-lights').empty().append(mkTableLights(d));
        $('#drop-zone-actions').empty().append(mkTableActions(d));
        $('#drop-zone-temp').empty().append(mkTableTarget('t',d));
        $('#drop-zone-humid').empty().append(mkTableTarget('h',d));
        $('#drop-zone-co2').empty().append(mkTableTarget('co2',d));
        $('.drop-zone').each(function () {
            $('table', this).prepend($('<caption/>', { text: $(this).attr('title') }));
        });

        if (!donotupdatesetting)
            sendSettings();
    }).error(function (jqXHR, textStatus, errorThrown) {
        console.log("get recipe failed");
    });
}

function zeroPad(num, size) {
    return (1e15 + num + "").slice(-size);
}
function formatDateTime(d) {
    // padding function
    var s = function (a, b) { return (1e15 + a + "").slice(-b) };

    // default date parameter
    if (typeof d === 'undefined') {
        d = new Date();
    };

    // return ISO datetime
    return d.getFullYear() + '-' +
        s(d.getMonth() + 1, 2) + '-' +
        s(d.getDate(), 2) + ' ' +
        s(d.getHours(), 2) + ':' +
        s(d.getMinutes(), 2)
    //+ ':' +        s(d.getSeconds(), 2)
    ;
}
//Several versions of drawChart... drawChartHTML puts in divs - fastest, most responsive but will not do ramps
// svg animation too slow
// canvas animation limited
function drawChartHTML(d) {
    $('#area-chart-svg').hide();
    var div = $('#area-chart-html').show();
    div.empty();
    var xmax = d.segments[d.segments.length - 1][1];
    var spaceForActions = 100;
    if (!d.actions || d.actions.length == 0)
        spaceForActions = 0;
    var ymax = div.height();
    if (xmax > 100)
        div.css({ width: 1700, 'over-flow': 'auto' });
    var xs = div.width() / xmax;
    for (var i = 0; i < d.segments.length; i++) {
        var x = d.segments[i][0] * xs;
        var ybase = 0;
        $('<div/>', {
            css: {
                position: 'absolute',
                top: ymax + 2 * bdr - 3,
                left: x + bdr - 2,
                width: xs * (d.segments[i][1] - d.segments[i][0]) - bdr,
                height: 10,
                'background-color': function (t) {
                    var colLst = [];
                    for (var j = 0; j < 3; j++)
                        colLst.push(Math.floor(256 * t[3 + 2 * j] * t[4 + 2 * j] / 100 / 500));
                    return ('rgb(' + (colLst.join(',')) + ')');
                }(d.segments[i]),
                'border-width': 1,
                'border-color': 'black',
            }
        }).appendTo(div);
        for (var j = 0; j < d.cols.length; j++) {
            var h = ys * d.segments[i][3 + 2 * j] * d.segments[i][4 + 2 * j] / 100;
            var r = $('<div/>', {
                css: {
                    'border-width': bdr + "px",
                    position: 'absolute',
                    top: ymax,
                    left: x,
                    width: xs * (d.segments[i][1] - d.segments[i][0]) - bdr,
                    height: 1,
                    'background-color': knownColorsH[d.cols[j]].show,
                    opacity: 1
                },
                'data-line': i, 'class': 'col-' + i + ' row-' + j,
                'data-len': j,
                click: function () {
                    var tdi = $('#cc_' + $(this).data('line') + "_0");
                    $('#drop-zone-lights').animate({ scrollTop: tdi.offset().top - tdi.parents('table').offset().top - 5 });
                    $('.lg-selected').removeClass('lg-selected');
                    tdi.parents('tr').addClass('lg-selected');
                }
            })
                .delay(20 * (i + j))
                .animate({ height: h - bdr, 'margin-top': -ybase - h }, { duration: 500 })
                .appendTo(div);
            ybase += h;
        }
    }
    var knownActions = { W: { t: 'water', h: 5 }, C: { t: 'clean', h: 25 }, I: { t: 'inspect', h: 50 }, H: { t: 'harvest', h: 50 } };
    if (d.actions)
        for (var i = 0; i < d.actions.length; i++) {
            var x = d.actions[i][0] * xs;
            var act = knownActions[d.actions[i][1]] || { t: '', h: 75 };
            $('<div/>', {
                css: {
                    top: act.h,
                    left: x + 'px',
                },
                text: d.actions[i][1].toLowerCase(),
                title: d.actions[i][2],
                'class': 'action-tag',
                id: 'at_' + i,
                data: { i: i, xs: xs },
                click: function () {
                    $('.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var tb = $('#action_' + $(this).data('i') + '_0');
                    //                    tb.get(0).scrollIntoView();
                    $('#drop-zone-actions').animate({ scrollTop: tb.offset().top - tb.parents('table').offset().top - 5 });
                    $('.at-selected').removeClass('at-selected');
                    tb.parents('tr').addClass('at-selected');
                }
            }).appendTo(div);
        }
    $('.action-tag').draggable({
        axis: 'x', drag: function () {
            var frm = parseInt($(this).css('left').replace(/px/g, '')) / $(this).data('xs');
            $('#action_' + $(this).data('i') + '_0').val(secondsToDHM(Math.floor(frm)));
        }
    });
    drawTargets(d);
}
function drawTargets(d) {
    var div = $('#area-chart-html').show();
    var xmax = d.segments[d.segments.length - 1][1];
    var spaceForActions = 100;
    if (!d.actions || d.actions.length == 0)
        spaceForActions = 0;
    var ymax = div.height();
    if (xmax > 100)
        div.css({ width: 1700, 'over-flow': 'auto' });
    var xs = div.width() / xmax;

    if (d.targets) {        
        var env = $('<div/>', { id: 'env-container', css: { top: spaceForActions } });
        for (var prmi = 0; prmi < prms.length; prmi++) {
            var elems = d.targets.filter(function (x) { return x[1] == prms[prmi].prm });
            if (elems.length == 0) continue;
            var vals = elems.map(function (x) { return x[2]; });
            var max = vals.reduce(function (p, c) { return Math.max(p, c); });
            var min = vals.reduce(function (p, c) { return Math.min(p, c); });
            var yts = -100 / (max - min + .001);
            var yt0 = max / yts + spaceForActions;
            for (var i = 0; i < elems.length; i++) {
                var cv = (elems[i][2] - min) * yts + yt0;
                var rhs = (i == elems.length - 1) ? xmax : elems[i + 1][0];
                $('<div/>', {
                    css: {
                        position: 'absolute', top: cv,
                        left: elems[i][0] * xs,
                        width: (rhs - elems[i][0]) * xs, border: 'solid thick ' + prms[prmi].color, height: 0
                    }
                }).appendTo(env);
                if (i == elems.length - 1) continue;
                var nv = (elems[i + 1][2] - min) * yts + yt0;
                var lo = Math.min(cv, nv);
                var hi = Math.max(cv, nv);
                $('<div/>', {
                    css: {
                        position: 'absolute', top: lo,
                        left: rhs * xs,
                        height: hi - lo, border: 'solid thick ' + prms[prmi].color, width: 0
                    }
                }).appendTo(env);
            }
        }
        env.appendTo(div);
    }
}

function drawChartHTMLNew(d) {
    $('#area-chart-svg').hide();
    var div = $('#area-chart-html').show();
    div.empty();
    var xmax = d.segments[d.segments.length - 1][1];
    var spaceForActions = 100;
    if (!d.actions || d.actions.length == 0)
        spaceForActions = 0;
    var ymax = div.height();
    if (xmax > 100)
        div.css({width:1700,'over-flow':'auto'});
    var xs = div.width() / xmax;
    for (var i = 0; i < d.segments.length; i++) {
        var x = d.segments[i][0] * xs;
        var ybase = 0;
        $('<div/>', {
            css: {
                position: 'absolute',
                top: ymax + 2 * bdr - 3,
                left: x + bdr - 2,
                width: xs * (d.segments[i][1] - d.segments[i][0]) - bdr,
                height: 10,
                'background-color': function (t) {
                    var colLst = [];
                    for (var j = 0; j < 3; j++)
                        colLst.push(Math.floor(256 * t[3 + 2 * j] * t[4 + 2 * j] / 100 / 500));
                    return ('rgb(' + (colLst.join(',')) + ')');
                }(d.segments[i]),
                'border-width': 1,
                'border-color': 'black',
            }
        }).appendTo(div);
        for (var j = 0; j < d.cols.length; j++) {
            var h = ys * d.segments[i][3 + 2 * j] * d.segments[i][4 + 2 * j] / 100;
            var r = $('<div/>', {
                css: {
                    'border-width': bdr + "px",
                    position: 'absolute',
                    top: ymax,
                    left: x,
                    width: xs * (d.segments[i][1] - d.segments[i][0]) - bdr,
                    height: 1,
                    'background-color': knownColorsH[d.cols[j]].show,
                    opacity: 1
                },
                'data-line': i, 'class':'col-'+i+' row-'+j,
                'data-len': j,
                click: function () {
                    var tdi = $('#cc_' + $(this).data('line') + "_0");
                    $('#drop-zone-lights').animate({scrollTop:tdi.offset().top-tdi.parents('table').offset().top-5});
                    $('.lg-selected').removeClass('lg-selected');
                    tdi.parents('tr').addClass('lg-selected');
                }
            })
                .delay(20 * (i + j))
                .animate({ height: h - bdr, 'margin-top': -ybase - h }, { duration: 500 })
                .appendTo(div);
            ybase += h;
        }
    }
    var knownActions = { W: { t: 'water', h: 5 }, C: { t: 'clean', h: 25 }, I: { t: 'inspect', h: 50 }, H: {t:'harvest',h:50} };
    if (d.actions)
        for (var i = 0; i < d.actions.length; i++) {
            var x = d.actions[i][0] * xs;
            var act = knownActions[d.actions[i][1]] || { t: '', h: 75 };
            $('<div/>', {
                css: {
                    top: act.h,
                    left: x+'px',
                },
                text: d.actions[i][1].toLowerCase(),
                title: d.actions[i][2],
                'class': 'action-tag',
                id: 'at_' + i,
                data : {i:i,xs:xs},
                click: function () {
                    $('.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var tb = $('#action_' + $(this).data('i') + '_0');
//                    tb.get(0).scrollIntoView();
                    $('#drop-zone-actions').animate({ scrollTop: tb.offset().top - tb.parents('table').offset().top - 5 });
                    $('.at-selected').removeClass('at-selected');
                    tb.parents('tr').addClass('at-selected');
                }
            }).appendTo(div);
        }
    $('.action-tag').draggable({
        axis: 'x', drag: function () {
            var frm = parseInt($(this).css('left').replace(/px/g, '')) / $(this).data('xs');
            $('#action_' + $(this).data('i') + '_0').val(secondsToDHM(Math.floor(frm)));
        }
    });
    if (d.targets) {
        var prms = [{ prm: 't', text: 'temperature', color: 'rgba(128,0,0,.5)' },
            { prm: 'h', text: 'humidity', color: 'rgba(0,128,0,.5)' },
            { prm: 'co2', text: 'CO2', color: 'rgba(0,0,128,.5)' }];
        var env = $('<div/>', { 'class': 'env-container', css: { top: spaceForActions } });
        for (var prmi = 0; prmi < prms.length; prmi++) {
            var elems = d.targets.filter(function (x) { return x[1] == prms[prmi].prm });
            if (elems.length==0) continue;
            var vals = elems.map(function (x) { return x[2]; });
            var max = vals.reduce(function (p, c) { return Math.max(p, c); });
            var min = vals.reduce(function (p, c) { return Math.min(p, c); });
            var ys = -100/(max - min + .001);
            var y0 = max/ys + spaceForActions;
            for (var i = 0; i < elems.length; i++) {
                var cv = (elems[i][2]-min)*yts+yt0;
                var rhs = (i == elems.length - 1) ? xmax : elems[i + 1][0];
                $('<div/>', {
                    css: {
                        position: 'absolute', top: cv,
                        left:elems[i][0]*xs,
                        width: (rhs-elems[i][0])*xs, border: 'solid thick ' + prms[prmi].color, height: 0
                    }
                }).appendTo(env);
                if (i == elems.length - 1) continue;
                var nv = (elems[i + 1][2]-min) * yts + yt0;
                var lo = Math.min(cv, nv);
                var hi = Math.max(cv, nv);
                $('<div/>', {
                    css: {
                        position: 'absolute', top: lo,
                        left: rhs * xs,
                        height: hi-lo, border: 'solid thick ' + prms[prmi].color, width: 0
                    }
                }).appendTo(env);

            }
        }
        //div.append(env);
    }
}

function drawChartSVG(d) {
    var svg = $('#area-chart-svg');
    svg.attr('viewBox', "0 0 " + svg.width() + " 700");
    svg.empty();
    //$SVG('circle', {
    //    r: 50,
    //    stroke: 'red',
    //    'stroke-width': 3
    //})
    //.appendTo(svg);
    var g = $SVG('g', {
        transform: 'translate(0,300) scale(1,-1)',
        stroke: 'gray', 'stroke-width': 4
    }).appendTo(svg);
    var xmax = d.segments[d.segments.length - 1][1];
    var xs = svg.width() / xmax;
    var ys = 300 / 1500;
    var char = 0;
    for (var i = 0; i < d.segments.length; i++) {
        var x = d.segments[i][0] * xs;
        var ybase = 0;
        var g1 = $SVG("g", { transform: 'translate(' + x + ',0)' }).appendTo(g);
        var g2 = $SVG('g', { transform: 'scale(1, 0)' }).appendTo(g1);
        var ani = $SVG('animateTransform', {
            attributeName: "transform",
            attributeType: "XML",
            'type': "scale",
            from: "1 00",
            to: "1 1",
            dur: "0.1s",
            fill: 'freeze',
            repeatCount: "1",
            begin: (0 + 0.1 * i) + "s"
        });
        ani.appendTo(g2)
        for (var j = 0; j < d.cols.length; j++) {
            var h = ys * d.segments[i][3 + 2 * j] * d.segments[i][4 + 2 * j] / 100;
            var r = $SVG('rect', {
                y: ybase, width: xs * (d.segments[i][1] - d.segments[i][0]), height: h,
                fill: knownColorsH[d.cols[j]].show,
                'data-char': i, 'data-len': j
            })
                //.append($SVG('animate', {attributeType:"XML",
                // attributeName:"fill",
                // to:transCol(d.cols[j]),
                // begin:(i/5)+"s"
                // }))
                .appendTo(g2);
            ybase += h;
        }
    }
    $('rect').click(function () {

    });
}

function drawChartCanvas(d) {
    var ctx = document.getElementById("area-chart").getContext("2d");
    var data = {
        labels: [],
        datasets: []
    };

    for (var j = 0; j < d.cols.length; j++)
        data.datasets[j] = {
            label: d.cols[j],
            data: [],
            strokeColor: knownColorsH[d.cols[j]].show,
            pointColor: knownColorsH[d.cols[j]].show
        }
    data.datasets[d.cols.length] = {
        label: 'total',
        data: [],
        strokeColor: 'black',
        pointColor: 'black'
    };
    for (var i = 0; i < d.segments.length; i++) {
        data.labels[i] = d.segments[i][0];
        var tot = 0;
        for (var j = 0; j < d.cols.length; j++) {
            var h = d.segments[i][3 + 2 * j] * d.segments[i][4 + 2 * j] / 100;
            tot += h;
            data.datasets[j].data[i] = h;
        }
        data.datasets[d.cols.length].data[i] = tot;
    }
    console.log(data);
    var options = {
        bezierCurve: false, datasetFill: false,
    };
    var areaChart = new Chart(ctx).Line(data, options);
}

function fillTextarea(d) {
    //Fill the text box
    var hdr = "int\tprf";
    for (var i = 0; i < d.cols.length; i++) {
        hdr += "\t" + knownColorsH[d.cols[i]].ola + ".c\t" + knownColorsH[d.cols[i]].ola + ".d";
    }
    var lns = [hdr];
    var acc = hdr.length + 1;
    var charPosn = [acc];
    for (var i = 0; i < d.segments.length; i++) {
        var ln = (d.segments[i][1] - d.segments[i][0]) + "\t" + d.segments[i][2];
        for (var j = 0; j < d.cols.length; j++) {
            ln += "\t" + d.segments[i][3 + 2 * j] + "\t" + d.segments[i][4 + 2 * j];
        }
        lns.push(ln);
        acc += ln.length + 1;
        charPosn.push(acc);
    }
    //Do the drag drop code
    //$('#raw-text').val(lns.join("\n"));
    //$('#raw-text').data('charPosn', charPosn);
}

function $SVG(tag, attr) {
    if (tag[0] == '<')
        tag = tag.substring(1);
    if (tag.substring(tag.length - 2) == "/>")
        tag = tag.substring(0, tag.length - 2);
    var ret = $(document.createElementNS('http://www.w3.org/2000/svg', tag));
    if (attr)
        for (var k in attr)
            ret.get(0).setAttribute(k, attr[k]);
    return ret;
}

$.fn.selectRange = function (start, end) {
    if (!end) end = start;
    return this.each(function () {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

////////////// Drag & drop
var holder = document.getElementById('drop-zone-lights'),
    state = document.getElementById('status');

if (typeof window.FileReader === 'undefined') {
    state.className = 'fail';
} else {
    state.className = 'success';
    state.innerHTML = 'File API & FileReader available';
}

holder.ondragover = function () {
    $(this).addClass('hover');
    return false;
};
holder.ondragend = function () {
    $(this).removeClass('hover');
    return false;
};
holder.ondrop = function (e) {
    this.className = '';
    e.preventDefault();

    var file = e.dataTransfer.files[0],
        reader = new FileReader();
    reader.onload = function (event) {
        console.log(event.target);
        $('#raw-text').val(event.target.result);
        $('#button-parse').trigger('click');
    };
    console.log(file);
    reader.readAsText(file);
    return false;
};