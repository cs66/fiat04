﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using LghtCtrl;
using CalTest;

namespace editor
{
    public partial class data : System.Web.UI.Page
    {
        static String constr = "Data Source=.\\sqlexpress;Initial Catalog=igsgs;User ID=sysdba;Password=masterkey";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (File.Exists(Server.MapPath("connectionstring.txt")))
                constr = File.ReadAllText(Server.MapPath("connectionstring.txt"));
            Response.ContentType = "application/json";
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            if (Request["action"] == "login")
            {
                String sql = String.Format(@"
SELECT id,admin FROM usr WHERE id={0} AND passwd = CONVERT(VARCHAR(32), HashBytes('MD5', {1}), 2)",
                    qr("usr"), qr("passwd"));
                Dictionary<String, Object> res = sqlToSingle(sql,con);
                if (res == null)
                {
                    Response.Write("{\"err\":\"Wrong user name/password\"}");
                }
                else
                {
                    long rand = (new Random()).Next();
                    Response.Cookies["token"].Value = rand.ToString();
                    Response.Cookies["token"].Expires = DateTime.Now.AddMonths(3);
                    res.Add("token", Response.Cookies["token"].Value);
                    Response.Cookies["usr"].Value = Request.QueryString["usr"];
                    Response.Cookies["usr"].Expires = DateTime.Now.AddMonths(3);
                    Response.Cookies["admin"].Value = Request.QueryString["admin"];
                    Response.Cookies["admin"].Expires = DateTime.Now.AddMonths(3);
                    sql = String.Format("INSERT INTO session(token,usr,IP) VALUES ({0},{1},{2})",
                        rand, qr("usr"), q("nix"));
                    SqlCommand com = new SqlCommand(sql, con);
                    com.ExecuteNonQuery();
                    Response.Write(ToJSON(res));
                }
                return;
            }
            if (Request["action"] == "logout")
            {
                String sql = String.Format(@"DELETE FROM session WHERE token={0}",q(Request.Cookies["token"].Value));
                SqlCommand com = new SqlCommand(sql, con);
                com.ExecuteNonQuery();
                Response.Cookies["token"].Expires = DateTime.Now.AddDays(-1);
                con.Close();
                return;
            }
            //Check authentication
            String usr = Request.Cookies["usr"]!=null?Request.Cookies["usr"].Value:null;
            String auth = String.Format("SELECT session.usr,COALESCE(admin,'n') as admin,COALESCE(settings,'{{}}') as settings FROM session JOIN usr ON session.usr=usr.id WHERE session.usr={0} and token={1}",
                q(usr), q(Request.Cookies["token"]!=null?Request.Cookies["token"].Value:null));
            Dictionary<String, Object> authObj = sqlToSingle(auth, con);
            if (authObj==null)
            {
                Response.Write("{\"err\":\"not authorised\"}");
                con.Close();
                return;
            }
            usr = (String)authObj["usr"];
            Boolean admin = ((String)authObj["admin"] == "y");

            // Keep these ordered alphabetically on action value
            if (Request["action"] == "get-recipe")
            {
                String sql;
                if (Request["rnm"] != null)
                    sql = String.Format("SELECT TOP 1 rid,rnm,ver,usr,whn FROM recipe WHERE rnm={0} ORDER BY rid DESC",
                    q(Request["rnm"]));
                else
                    sql = String.Format("SELECT TOP 1 rid,rnm,ver,usr,whn FROM recipe WHERE rid={0} ORDER BY rid DESC",
                    q(Request["rid"]));
                Dictionary<String, Object> res = sqlToSingle(sql, con);
                //Get distinct cols starting with red, green, blue then in alphabetical order
                if (res == null)
                {
                    Response.Write("{\"err\":\"no such recipe\"}");
                    con.Close();
                    return;
                }
                List<Object> cols = sqlToColList(String.Format(@"
SELECT col FROM (
	SELECT DISTINCT col FROM segcol WHERE rid={0}) AS s
 ORDER BY CASE WHEN col='red' THEN '1' WHEN col='green' THEN '2' WHEN col='blue' THEN '3' ELSE col END", res["rid"]),
                    con);
                res.Add("cols", cols);
                sql = "SELECT segment.frm,utl,prf";
                foreach (Object o in cols)
                {
                    String col = (String)o;
                    sql += String.Format(@"  ,MAX(CASE WHEN col='{0}' THEN crr END) AS {0}_cur  ,MAX(CASE WHEN col='{0}' THEN dty END) AS {0}_dty ", col);
                }
                sql += String.Format(@" FROM segment JOIN segcol ON segment.frm=segcol.frm AND segment.rid={0} AND segcol.rid={0} GROUP BY segment.frm,utl,rmp,prf", res["rid"]);
                res.Add("segments", sqlToListList(sql, con));
                if (res["rid"] != "undefined")
                {
                    sql = String.Format(@"SELECT frm,act,msg FROM actions WHERE rid={0} ORDER BY frm", res["rid"]);
                    res.Add("actions", sqlToListList(sql, con));
                    sql = String.Format(@"SELECT frm,prm,tgt FROM targetenv WHERE rid={0} ORDER BY frm", res["rid"]);
                    res.Add("targets", sqlToListList(sql, con));
                }
                Response.Write(ToJSON(res));
                con.Close();
                return;
            }// End get-recipe

            if (Request["action"] == "get-recipe-list")
            {
                String term = Request["term"];
                if (term == null)
                    term = "";
                term = term.Replace("'", "''");
                String sql = String.Format("SELECT rid,rnm FROM recipe WHERE rnm LIKE '%{0}%' ORDER BY rnm",
                    term);
                SqlCommand com = new SqlCommand(sql, con);
                List<Object> res = new List<Object>();
                SqlDataReader sdr = com.ExecuteReader();
                while (sdr.Read())
                {
                    Dictionary<String, Object> item = new Dictionary<String, Object>();
                    item.Add("id", sdr["rid"].ToString());
                    item.Add("label", sdr["rnm"].ToString());
                    res.Add(item);
                }
                Response.Write(ToJSON(res));
                con.Close();
                return;
            } // End get-recipe-list

            if (Request["action"] == "get-site-settings")
            {
                String sql = String.Format(@"
SELECT mrupdate, settings
  FROM site WHERE id={0}
",
                    qr("site"));
                Dictionary<String,Object> d = sqlToSingle(sql,con);
                if (Request["mrupdate"] != null && Request["mrupdate"] == d["mrupdate"].ToString())
                {
                    Response.Write("{\"use-cache\":\"nothing new\"}");
                }
                else
                {
                    Response.Write(String.Format("{{\"mrupdate\":\"{0}\",\"settings\":{1}}}",d["mrupdate"].ToString(),(String)d["settings"]));
                }
                con.Close();
                return;
            } // End get-site-settings

            if (Request["action"] == "get-thco2") // Temperature, humidity, CO2
            {
                String sql = String.Format(@"
select 10000*year(whn)+100*month(whn)+day(whn),min(whn),count(1),
	min(temp),max(temp),avg(temp),
	min(humidity),max(humidity),avg(humidity),
	min(co2),max(co2),avg(co2)
 from thco2
 where site = {0} AND whn between {1} and {2}
 group by 10000*year(whn)+100*month(whn)+day(whn)", qr("site"), qr("lo"), qr("hi"));
                Response.Write(ToJSON(sqlToListList(sql, con)));
                con.Close();
                return;
            } // End get thco2

            if (Request["action"] == "get-trials")
            {
                String sql = String.Format(@"SELECT tid,start,trial.rid,[tray],recipe.rnm,times.ln
  FROM trial JOIN recipe ON trial.rid=recipe.rid JOIN (SELECT rid, MAX(utl) ln FROM segment GROUP BY rid) AS times ON trial.rid=times.rid
  WHERE start BETWEEN {0} and {1} AND [site]={2}",
                    qr("from"),qr("to"),qr("site"));
                Response.Write(ToJSON(sqlToListList(sql,con)));
                return;
            } // End get-trials

            if (Request["action"] == "get-trial-mass")
            {
                String sql = String.Format("SELECT whn,wgt FROM growth WHERE tid={0}", qr("tid"));
                Response.Write(ToJSON(sqlToListList(sql, con)));
                return;
            }// End get-trial-mass

            if (Request["action"] == "get-trial-attachment")
            {
                String sql = String.Format("SELECT whn,typ,fnm FROM attachment WHERE tid={0}", qr("tid"));
                Response.Write(ToJSON(sqlToListList(sql, con)));
                return;
            }// End get-trial-attachment

            if (Request["action"] == "nuke-cal")
            {
                CalTest.Program.NukeCal(Server.MapPath("client_secret.json"));
                Response.Write("{}");
                return;
            }
            if (Request["action"] == "save-recipe")
            {
                String sql = String.Format("SELECT COALESCE(MAX(rid),0) FROM recipe WHERE rnm = {0} AND usr = {1}", qr("rnm"), q(usr));
                SqlCommand com = new SqlCommand(sql, con);
                int row = (int)com.ExecuteScalar();
                if (row == 0)
                {
                    com.CommandText = String.Format("INSERT INTO recipe(rnm,usr,ver,whn) VALUES ({0},{1},1,CURRENT_TIMESTAMP)",
                        qr("rnm"), q(usr));
                    com.ExecuteNonQuery();
                    com.CommandText = "SELECT @@IDENTITY";
                    row = (int)(Decimal)com.ExecuteScalar();
                }
                String [] cols = Request["cols"].Split(',');
                String[] segs = Request["segs"].Split('\n');
                com.CommandText = String.Format("DELETE FROM segcol WHERE rid={0}", row);
                com.ExecuteNonQuery();
                com.CommandText = String.Format("DELETE FROM segment WHERE rid={0}", row);
                com.ExecuteNonQuery();
                foreach (String seg in segs)
                {
                    String[] nums = seg.Split(',');
                    com.CommandText = String.Format("INSERT INTO segment(rid,frm,utl,prf,prd) VALUES ({0},{1},{2},{3},{4})",
                        row, qn(nums[0]), qn(nums[1]), qn(nums[2]), 0); // Replace 0 with prd one day
                    
                    com.ExecuteNonQuery();
                    for (int i = 0; i < cols.Length; i++)
                    {
                        com.CommandText = String.Format("INSERT INTO segcol(rid,frm,col,crr,dty) VALUES ({0},{1},{2},{3},{4})",
                            row,qn(nums[0]),q(cols[i]),qn(nums[3+2*i]),qn(nums[4+2*i])); // Replace 0 with rmp one day
                        com.ExecuteNonQuery();
                    }
                }
                Response.Write("{\"rid\":" + row + "}");
                con.Close();
                return;
            } // End save-recipe

            if (Request["action"] == "update-settings")
            {
                String sql = String.Format("UPDATE session SET settings = {0} WHERE usr = {1}", qr("settings"), q(usr));
                SqlCommand com = new SqlCommand(sql, con);
                com.ExecuteNonQuery();
                Response.Write(Request["settings"]);
                con.Close();
                return;
            } //End update-settings

            if (Request["action"] == "update-site")
            {
                String sql = String.Format("UPDATE site SET settings = {0} WHERE id = {1} AND {2} IN (SELECT usr FROM siteOwner WHERE site={1})", qr("settings"), qr("site"),q(usr));
                SqlCommand com = new SqlCommand(sql, con);
                com.ExecuteNonQuery();
                Response.Write(Request["settings"]);
                Object stg = LghtCtrl.JSONReader.ParseJSON(Request["settings"]);
                List<Object> trays = (List<Object>)JSONReader.get(stg, "trays");
                String recipe = null;
                String startAt = null;
                foreach (Object t in trays)
                {
                    if (((String)JSONReader.get(t,"id")) == Request["addr"] &&
                        JSONReader.ContainsKey(t,"recipe"))
                    {
                        recipe = (String)JSONReader.get(t, "recipe");
                        startAt = (String)JSONReader.get(t, "startAt");
                        break;
                    }
                }
                String tower = (Request["addr"].Split('-'))[0];
                List<Object> subscribersO = (List<Object>)JSONReader.get(JSONReader.get(JSONReader.get(stg, "towers"),tower),"subscribers");
                List<String> subscribers = new List<String>();
                foreach (Object o in subscribersO)
                    subscribers.Add((String)o);
                if (recipe != null)
                {
                    sql = String.Format("SELECT frm,act,COALESCE(msg,'')  FROM actions WHERE rid={0}", recipe);
                    com.CommandText = sql;
                    SqlDataReader sdr = com.ExecuteReader();
                    List<Int32> times = new List<int>();
                    List<String> acts = new List<string>();
                    List<String> msgs = new List<string>();
                    while (sdr.Read())
                    {
                        times.Add(sdr.GetInt32(0));
                        acts.Add(sdr.GetString(1));
                        msgs.Add(sdr.GetString(2));
                    }
                    CalTest.Program.CreateEvents(Request["addr"], times, acts, msgs, startAt, recipe, Server.MapPath("client_secret.json"), subscribers);
                }
                con.Close();
                return;
            }
            if (Request["action"] == "who-am-i")
            { // Note that users could fake being admin by fixing cookies - that gives them access to the UI components but the admin rights get checked when they try to do something
                List<Dictionary<String,Object>> sites = sqlToListObj(String.Format("SELECT site FROM siteOwner WHERE usr={0}",q(usr)),con);
                List<String> ssite = new List<string>();
                foreach (Dictionary<String,Object> s in sites){
                    ssite.Add("\"" + (String)s["site"] + "\"");
                }
                Response.Write("{\"id\":\"" + usr + "\",\"admin\":\"" + admin + "\",\"settings\":" + ((String)authObj["settings"]) + 
                    ",\"sites\":["+ String.Join(",",ssite.ToArray())+ "]"+  "}");
                con.Close();
                return;
            } // End who-am-i
         
   
            // Drops through to error
            Response.Write("{\"err\":\"Unknown action\"}");
            con.Close();
            return;
        }
        List<Object> sqlToColList(String sql, SqlConnection con)
        {
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            List<Object> res = new List<Object>();
            while (sdr.Read())
            {
                Object v = null;
                if (sdr.IsDBNull(0))
                    v = null;
                else
                    v = sdr.GetValue(0);
                if (sdr.GetDataTypeName(0) == "int")
                    v = sdr.GetInt32(0);
                res.Add(v);
            }
            sdr.Close();
            return res;
        }
        List<Dictionary<String,Object>> sqlToListObj(string sql, SqlConnection con)
        {
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            List<Dictionary<String, Object>> res = new List<Dictionary<String, Object>>();
            while (sdr.Read())
            {
                Dictionary<String, Object> item = new Dictionary<string, object>();
                for (int i = 0; i < sdr.FieldCount; i++)
                {
                    Object v = null;
                    if (sdr.IsDBNull(i))
                        v = null;
                    else
                        v = sdr.GetValue(i);
                    if (sdr.GetDataTypeName(i) == "int")
                        v = sdr.GetInt32(i);
                    item.Add(sdr.GetName(i), v);
                }
                res.Add(item);
            }
            sdr.Close();
            return res;
        }
        List<Object> sqlToListList(string sql, SqlConnection con)
        {
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            List<Object> res = new List<Object>();
            while (sdr.Read())
            {
                List<Object> item = new List<object>();
                for (int i = 0; i < sdr.FieldCount; i++)
                {
                    Object v = null;
                    if (sdr.IsDBNull(i))
                        v = null;
                    else
                        v = sdr.GetValue(i);
                    if (sdr.GetDataTypeName(i) == "int")
                        v = sdr.GetInt32(i);
                    if (sdr.GetDataTypeName(i) == "float")
                        v = sdr.GetDouble(i);
                    item.Add(v);
                }
                res.Add(item);
            }
            sdr.Close();
            return res;
        }

        Dictionary<String, Object> sqlToSingle(String sql, SqlConnection con)
        {
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            Dictionary<String,Object> res = new Dictionary<string,object>();
            if (sdr.Read())
                for (int i = 0; i < sdr.FieldCount; i++)
                {
                    Object v = null;
                    if (sdr.IsDBNull(i))
                        v = null;
                    else
                    {
                        v = sdr.GetValue(i);
                        if (sdr.GetDataTypeName(i) == "int")
                            v = sdr.GetInt32(i);
                        res.Add(sdr.GetName(i), v);
                    }
                }
            else
                return null;
            sdr.Close();
            return res;
        }
        String q(String s)
        {
            if (s == null)
                return "null";
            else
                return "'"+s.Replace("'", "''")+"'";
        }

        long qn(String s)
        {
            return Int32.Parse(s);
        }
        String qr(String s)
        {
                return q(Request[s]);
        }
        string ToJSON(Object o)
        {
            if (o == null)
                return "null";
            StringBuilder sb = new StringBuilder();
            String t = o.GetType().ToString();
            String sep = "";
            if (t.StartsWith("System.Collections.Generic.List"))
            {
                sb.Append("[");
                List<Object> lo = (List<Object>)o;
                foreach (Object m in lo)
                {
                    sb.Append(sep + ToJSON(m));
                    sep = ",";
                }
                sb.Append("]");
                return sb.ToString();
            }
            if (t.StartsWith("System.Collections.Generic.Dictionary"))
            {
                sb.Append("{");
                Dictionary<String, Object> lo = (Dictionary<String, Object>)o;
                foreach (String s in lo.Keys)
                {
                    sb.AppendFormat(sep + "\"{0}\":{1}", s, ToJSON(lo[s]));
                    sep = ",";
                }
                sb.Append("}");
                return sb.ToString();
            }
            if (t.StartsWith("System.String"))
            {
                return "\"" + o.ToString() + "\"";
            }
            if (t.StartsWith("System.Int") || t.StartsWith("System.Double"))
                return o.ToString();
            if (t.StartsWith("System.DateTime"))
                return "\""+((DateTime)o).ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz")+"\"";
            return t.ToString();
        }
    }
}