﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Windows.Media.Imaging;
using System.Data.SqlClient;

namespace editor
{
    public partial class upload : System.Web.UI.Page
    {
        static String constr = "Data Source=.\\sqlexpress;Initial Catalog=igsgs;User ID=sysdba;Password=masterkey";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (File.Exists(Server.MapPath("connectionstring.txt")))
                constr = File.ReadAllText(Server.MapPath("connectionstring.txt"));
            Response.ContentType = "application/json";
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand com = new SqlCommand("", con);
            int tid;
            if (!int.TryParse(Request["tid"], out tid))
            {
                Response.Write("{\"err\":\"no tid given\"}");
                return;
            }
            String path = "c://attachments//";
            List<String> taken = new List<string>();
            for (var i = 0; i < Request.Files.Count;i++ )
            {
                var file = Request.Files[i];
                file.SaveAs(path + file.FileName);
                FileStream fs = new FileStream(path + file.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BitmapSource img = BitmapFrame.Create(fs);
                BitmapMetadata md = (BitmapMetadata)img.Metadata;
                String dtf = DateTime.Parse(md.DateTaken).ToString("o").Substring(0,22);
                String sql = String.Format("INSERT INTO attachment(tid,whn,typ,fnm) VALUES ({0},'{1}','img','{2}')",
                    tid, dtf, file.FileName);
                com.CommandText = sql;
                com.ExecuteNonQuery();
                com.CommandText = "SELECT @@IDENTITY";
                decimal v = (decimal)com.ExecuteScalar();
                taken.Add(String.Format("[\"{0}\",\"{1}\",{2}]", file.FileName, dtf,v));
            }
            Response.Write("[" + String.Join(",", taken));
            con.Close();
            return;
        }
    }
}