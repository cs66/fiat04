﻿namespace LghtCtrl
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.SiteTbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ServerTbx = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.UsrTbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.PwdTbx = new System.Windows.Forms.TextBox();
            this.PollRateTbx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SerialPortCBx = new System.Windows.Forms.ComboBox();
            this.SessionTokenTbx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ClearSerialListBtn = new System.Windows.Forms.Button();
            this.LoadPlanBtn = new System.Windows.Forms.Button();
            this.SerialTickerBtn = new System.Windows.Forms.Button();
            this.SerialOutLbx = new System.Windows.Forms.ListBox();
            this.SerialPollCbx = new System.Windows.Forms.CheckBox();
            this.SerialTmr = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.errLbx = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.GetPlanBtn = new System.Windows.Forms.Button();
            this.HTTP_StartSession = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.PlanInPlaceLbl = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.NetworkOKLbl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Site Name:";
            // 
            // SiteTbx
            // 
            this.SiteTbx.Location = new System.Drawing.Point(93, 19);
            this.SiteTbx.Name = "SiteTbx";
            this.SiteTbx.Size = new System.Drawing.Size(212, 20);
            this.SiteTbx.TabIndex = 1;
            this.SiteTbx.Text = "Buckstone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Server:";
            // 
            // ServerTbx
            // 
            this.ServerTbx.Location = new System.Drawing.Point(93, 46);
            this.ServerTbx.Name = "ServerTbx";
            this.ServerTbx.Size = new System.Drawing.Size(212, 20);
            this.ServerTbx.TabIndex = 3;
            this.ServerTbx.Text = "http://localhost:19473/data.aspx";
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(6, 232);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(312, 33);
            this.SaveBtn.TabIndex = 8;
            this.SaveBtn.Text = "Save to igsgs.ini";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password:";
            // 
            // UsrTbx
            // 
            this.UsrTbx.Location = new System.Drawing.Point(93, 72);
            this.UsrTbx.Name = "UsrTbx";
            this.UsrTbx.Size = new System.Drawing.Size(212, 20);
            this.UsrTbx.TabIndex = 5;
            this.UsrTbx.Text = "andrew";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "User Name:";
            // 
            // PwdTbx
            // 
            this.PwdTbx.Location = new System.Drawing.Point(93, 98);
            this.PwdTbx.Name = "PwdTbx";
            this.PwdTbx.PasswordChar = '*';
            this.PwdTbx.Size = new System.Drawing.Size(212, 20);
            this.PwdTbx.TabIndex = 7;
            this.PwdTbx.Text = "brazil";
            // 
            // PollRateTbx
            // 
            this.PollRateTbx.Location = new System.Drawing.Point(234, 124);
            this.PollRateTbx.Name = "PollRateTbx";
            this.PollRateTbx.Size = new System.Drawing.Size(71, 20);
            this.PollRateTbx.TabIndex = 10;
            this.PollRateTbx.Text = "115200";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Serial Poll Rate (ms):";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SerialPortCBx);
            this.groupBox1.Controls.Add(this.SessionTokenTbx);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ServerTbx);
            this.groupBox1.Controls.Add(this.PollRateTbx);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.SiteTbx);
            this.groupBox1.Controls.Add(this.PwdTbx);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SaveBtn);
            this.groupBox1.Controls.Add(this.UsrTbx);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 271);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Persistent Values (.ini)";
            // 
            // SerialPortCBx
            // 
            this.SerialPortCBx.FormattingEnabled = true;
            this.SerialPortCBx.Location = new System.Drawing.Point(184, 150);
            this.SerialPortCBx.Name = "SerialPortCBx";
            this.SerialPortCBx.Size = new System.Drawing.Size(121, 21);
            this.SerialPortCBx.TabIndex = 15;
            // 
            // SessionTokenTbx
            // 
            this.SessionTokenTbx.Location = new System.Drawing.Point(93, 176);
            this.SessionTokenTbx.Name = "SessionTokenTbx";
            this.SessionTokenTbx.Size = new System.Drawing.Size(212, 20);
            this.SessionTokenTbx.TabIndex = 14;
            this.SessionTokenTbx.Text = "null";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Session Token:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Serial Port:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.ClearSerialListBtn);
            this.groupBox2.Controls.Add(this.LoadPlanBtn);
            this.groupBox2.Controls.Add(this.SerialTickerBtn);
            this.groupBox2.Controls.Add(this.SerialOutLbx);
            this.groupBox2.Controls.Add(this.SerialPollCbx);
            this.groupBox2.Location = new System.Drawing.Point(342, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1146, 227);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Line Outputs";
            // 
            // ClearSerialListBtn
            // 
            this.ClearSerialListBtn.Location = new System.Drawing.Point(422, 15);
            this.ClearSerialListBtn.Name = "ClearSerialListBtn";
            this.ClearSerialListBtn.Size = new System.Drawing.Size(259, 23);
            this.ClearSerialListBtn.TabIndex = 4;
            this.ClearSerialListBtn.Text = "Clear Serial List (Forces resend of commands)";
            this.ClearSerialListBtn.UseVisualStyleBackColor = true;
            this.ClearSerialListBtn.Click += new System.EventHandler(this.ClearSerialListBtn_Click);
            // 
            // LoadPlanBtn
            // 
            this.LoadPlanBtn.Location = new System.Drawing.Point(244, 15);
            this.LoadPlanBtn.Name = "LoadPlanBtn";
            this.LoadPlanBtn.Size = new System.Drawing.Size(172, 23);
            this.LoadPlanBtn.TabIndex = 3;
            this.LoadPlanBtn.Text = "Load Plan (local)";
            this.LoadPlanBtn.UseVisualStyleBackColor = true;
            this.LoadPlanBtn.Click += new System.EventHandler(this.LoadPlanBtn_Click);
            // 
            // SerialTickerBtn
            // 
            this.SerialTickerBtn.Location = new System.Drawing.Point(135, 14);
            this.SerialTickerBtn.Name = "SerialTickerBtn";
            this.SerialTickerBtn.Size = new System.Drawing.Size(103, 23);
            this.SerialTickerBtn.TabIndex = 2;
            this.SerialTickerBtn.Text = "Serial Ticker";
            this.SerialTickerBtn.UseVisualStyleBackColor = true;
            this.SerialTickerBtn.Click += new System.EventHandler(this.SerialTickerBtn_Click);
            // 
            // SerialOutLbx
            // 
            this.SerialOutLbx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SerialOutLbx.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialOutLbx.FormattingEnabled = true;
            this.SerialOutLbx.ItemHeight = 11;
            this.SerialOutLbx.Location = new System.Drawing.Point(9, 46);
            this.SerialOutLbx.Name = "SerialOutLbx";
            this.SerialOutLbx.Size = new System.Drawing.Size(1131, 169);
            this.SerialOutLbx.TabIndex = 1;
            // 
            // SerialPollCbx
            // 
            this.SerialPollCbx.AutoSize = true;
            this.SerialPollCbx.Location = new System.Drawing.Point(6, 19);
            this.SerialPollCbx.Name = "SerialPollCbx";
            this.SerialPollCbx.Size = new System.Drawing.Size(123, 17);
            this.SerialPollCbx.TabIndex = 0;
            this.SerialPollCbx.Text = "Serial Timer Enabled";
            this.SerialPollCbx.UseVisualStyleBackColor = true;
            this.SerialPollCbx.CheckedChanged += new System.EventHandler(this.SerialPollCbx_CheckedChanged);
            // 
            // SerialTmr
            // 
            this.SerialTmr.Interval = 5000;
            this.SerialTmr.Tick += new System.EventHandler(this.SerialTickerBtn_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // errLbx
            // 
            this.errLbx.FormattingEnabled = true;
            this.errLbx.Location = new System.Drawing.Point(13, 290);
            this.errLbx.Name = "errLbx";
            this.errLbx.Size = new System.Drawing.Size(317, 173);
            this.errLbx.TabIndex = 13;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.GetPlanBtn);
            this.groupBox3.Controls.Add(this.HTTP_StartSession);
            this.groupBox3.Location = new System.Drawing.Point(342, 248);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(416, 215);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "HTTP Issues";
            // 
            // GetPlanBtn
            // 
            this.GetPlanBtn.Location = new System.Drawing.Point(9, 49);
            this.GetPlanBtn.Name = "GetPlanBtn";
            this.GetPlanBtn.Size = new System.Drawing.Size(173, 23);
            this.GetPlanBtn.TabIndex = 1;
            this.GetPlanBtn.Text = "Get Plan";
            this.GetPlanBtn.UseVisualStyleBackColor = true;
            this.GetPlanBtn.Click += new System.EventHandler(this.GetPlanBtn_Click);
            // 
            // HTTP_StartSession
            // 
            this.HTTP_StartSession.Location = new System.Drawing.Point(9, 20);
            this.HTTP_StartSession.Name = "HTTP_StartSession";
            this.HTTP_StartSession.Size = new System.Drawing.Size(173, 23);
            this.HTTP_StartSession.TabIndex = 0;
            this.HTTP_StartSession.Text = "Start Session";
            this.HTTP_StartSession.UseVisualStyleBackColor = true;
            this.HTTP_StartSession.Click += new System.EventHandler(this.HTTP_StartSession_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 494);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "label8";
            // 
            // PlanInPlaceLbl
            // 
            this.PlanInPlaceLbl.AutoSize = true;
            this.PlanInPlaceLbl.BackColor = System.Drawing.Color.Red;
            this.PlanInPlaceLbl.Location = new System.Drawing.Point(769, 264);
            this.PlanInPlaceLbl.Name = "PlanInPlaceLbl";
            this.PlanInPlaceLbl.Size = new System.Drawing.Size(16, 13);
            this.PlanInPlaceLbl.TabIndex = 16;
            this.PlanInPlaceLbl.Text = "   ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(792, 264);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Plan in place";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label9.Location = new System.Drawing.Point(792, 285);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Network OK";
            // 
            // NetworkOKLbl
            // 
            this.NetworkOKLbl.AutoSize = true;
            this.NetworkOKLbl.BackColor = System.Drawing.Color.Red;
            this.NetworkOKLbl.Location = new System.Drawing.Point(769, 285);
            this.NetworkOKLbl.Name = "NetworkOKLbl";
            this.NetworkOKLbl.Size = new System.Drawing.Size(16, 13);
            this.NetworkOKLbl.TabIndex = 18;
            this.NetworkOKLbl.Text = "   ";
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1500, 521);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.NetworkOKLbl);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.PlanInPlaceLbl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.errLbx);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ControlPanel";
            this.Text = "IGS Control Panel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ControlPanel_FormClosed);
            this.Load += new System.EventHandler(this.ControlPanel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SiteTbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ServerTbx;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UsrTbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox PwdTbx;
        private System.Windows.Forms.TextBox PollRateTbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button SerialTickerBtn;
        private System.Windows.Forms.ListBox SerialOutLbx;
        private System.Windows.Forms.CheckBox SerialPollCbx;
        private System.Windows.Forms.Timer SerialTmr;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ListBox errLbx;
        private System.Windows.Forms.Button LoadPlanBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button HTTP_StartSession;
        private System.Windows.Forms.TextBox SessionTokenTbx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button GetPlanBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label PlanInPlaceLbl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label NetworkOKLbl;
        private System.Windows.Forms.ComboBox SerialPortCBx;
        private System.Windows.Forms.Button ClearSerialListBtn;
    }
}