﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LghtCtrl
{
    public class JSONReader
    {
        public static String Stringify(Object o)
        {
            Dictionary<String, Object> dict = o as Dictionary<String, Object>;
            if (dict != null)
            {
                List<String> l = new List<String>();
                foreach (String k in dict.Keys)
                    l.Add(String.Format("{{\"{0}\":{1}}}",k,Stringify(dict[k])));
                return "{" + String.Join(",", l.ToArray()) + "}";
            }
            List<Object> lst = o as List<Object>;
            if (lst != null)
            {
                List<String> l = new List<string>();
                foreach (Object o2 in lst)
                    l.Add(Stringify(o2));
                return "[" + String.Join(",", l.ToArray()) + "]";
            }
            String s = o as String;
            if (s!=null){
                return "\"" + s + "\"";
            }
            return o.ToString();
        }
        public static Object get(Object o, String k)
        {
            return ((Dictionary<String,Object>)o)[k];
        }
        public static bool ContainsKey(Object o, String k)
        {
            return ((Dictionary<String, Object>)o).ContainsKey(k);
        }
        public static Object get(Object o,string k1,string k2){
            return get(get(o, k1), k2);
        }
        public static Object ParseJSONFromFile(String fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            String s = sr.ReadToEnd();
            sr.Close();
            return ParseJSON(s);
        }
        public static Object ParseJSON(String s)
        {
            return ParseN(tokenise(s));
        }
        static Object ParseN(List<Object> lst)
        {
            Object fst = lst[0];
            lst.RemoveAt(0);
            if (fst is long || fst is String)
                return fst;
            char c = (char)fst;
            if (c == '{')
            {
                Dictionary<String, Object> d = new Dictionary<string, object>();
                while (lst.Count > 0 && lst[0] is String)
                {
                    String k = (String)lst[0];
                    char colon = (char)lst[1];
                    lst.RemoveAt(0);
                    lst.RemoveAt(0);
                    d.Add(k, ParseN(lst));
                    char commaOrCloseBrace = (char)lst[0];
                    lst.RemoveAt(0);
                }
                return d;
            }
            if (c == '[')
            {
                List<Object> l = new List<object>();
                if (lst[0] is char && (char)lst[0] == ']')
                {
                    char closeSquare = (char)lst[0];
                    lst.RemoveAt(0);
                    return l;
                }
                l.Add(ParseN(lst));
                while (lst[0] is char)
                {
                    char commaOrCloseSq = (char)lst[0];
                    lst.RemoveAt(0);
                    if (commaOrCloseSq == ']')
                    {
                        return l;
                    }
                    else
                        l.Add(ParseN(lst));
                }
            }
            return null;
        }
        static List<Object> tokenise(String s)
        {
            List<Object> ret = new List<Object>();
            const int s_any = 0;
            const int s_str = 1;
            const int s_num = 2;
            Int16 state = 0;  // 0=ready for anything, 1=in string, 2=in number
            int p = 0;
            String wrd = "";
            long num = 0;
            while (p < s.Length)
            {
                char c = s[p++];
                if (state == s_any)
                {
                    if (c == '\"')
                    {
                        state = s_str;
                        wrd = "";
                        continue;
                    }
                    if (c >= '0' && c <= '9')
                    {
                        state = s_num;
                        num = c - '0';
                        continue;
                    }
                    ret.Add(c);
                }
                else if (state == s_str)
                {
                    if (c == '\"')
                    {
                        state = s_any;
                        ret.Add(wrd);
                        continue;
                    }
                    if (c == '\\')
                    {
                        wrd += '\"';
                        p++;
                        continue;
                    }
                    wrd += c;
                    continue;
                }
                else if (state == s_num)
                {
                    if (c >= '0' && c <= '9')
                    {
                        num = 10 * num + (c - '0');
                    }
                    else
                    {
                        ret.Add(num);
                        state = s_any;
                        p--;
                    }
                }
            }
            return ret;
        }
    }
}
