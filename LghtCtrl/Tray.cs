﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LghtCtrl
{
    class Tray
    {
        public String id;
        public long serialAddr;
        List<String> colors;
        public long recipe;//Never a valid recipe
        public DateTime startAt;
        public Boolean noRecipe = true;

        public static Dictionary<String, Tray> jsonToPlan(Object o)
        {
            Dictionary<String, Tray> ret = new Dictionary<string, Tray>();
            List<Object> trays = (List<Object>)JSONReader.get(o,"settings","trays");
            foreach(Object otray in trays){
                Tray ttray = new Tray();
                Dictionary<String, Object> dtray = (Dictionary<String, Object>)otray;
                ttray.id = (string)dtray["id"];
                ttray.serialAddr = long.Parse((string)dtray["serial"]);
                if (dtray.ContainsKey("recipe"))
                {
                    ttray.recipe = long.Parse((string)dtray["recipe"]);
                    ttray.noRecipe = false;
                }
                if (dtray.ContainsKey("startAt"))
                    ttray.startAt = DateTime.Parse((String)dtray["startAt"]);
                ret.Add(ttray.id.ToString(), ttray);
            }
            return ret;
        }
    }
}
