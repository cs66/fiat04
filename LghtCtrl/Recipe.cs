﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LghtCtrl
{
    class Recipe
    {
        long id;
        Dictionary<long, String> segs;
        List<long> sseg;

        // Get Recipe from the server
        public Recipe(String server, String token, long id)
        {

        }
        public Recipe(String cacheDir,long id)
        {
            this.id = id;
            this.segs = new Dictionary<long,string>();
            this.sseg = new List<long>();
            String cacheFN = cacheDir + id.ToString()+".json";
            Object orecipe = JSONReader.ParseJSONFromFile(cacheFN);
            List<Object> lsegs = (List<Object>)((Dictionary<String, Object>)orecipe)["segments"];
            int segCount = 0;
            foreach (Object oseg in lsegs)
            {
                List<Object> seg = (List<Object>)oseg;
                long frm = (long)seg[0];
                long utl = (long)seg[1];
                long prf = (long)seg[2];
                String ln = "1A" + // Command Type - 1A is write;
                    "01" + // Blocks to write
                    "01" + // Number of blocks to write
                    "0004" + // Address to start writing
                    "002E" + // Number of bytes to write
                    "0001" + // Linearity
                    "00000001" + // Scaling factor
                    "0000" + // Phase Red
                    "0000" + // Phase Green
                    "0000" + // Phase Blue
                    "";
                string duty = "";
                string setpoint = "";
                for (int i = 3; i < seg.Count; i += 2)
                {
                    duty += string.Format("{0,4:X}", seg[i+1]).Replace(' ', '0');
                    setpoint += string.Format("{0,4:X}", (long)seg[i]).Replace(' ', '0'); // Percentage of 500mA
                    //ln += ((long)seg[i]).ToString() + " " + ((long)seg[i+1]).ToString() + " ";
                }
                ln += duty + // Percentage of duty cycle RGB
                    "0000" + // Profile Polarity
                    String.Format("{0,4:X}",prf).Replace(' ','0') + // Sync Frequency
                    "0000" + // Enable operation
                    "00000000" + // Time base value
                    setpoint + // Current level RGB
                    "0019" + // Time Constant ratio
                    "0004" + // Profile type (4=no ramp)
                    string.Format("{0,8:X}", (utl-frm) * 2).Replace(' ', '0') + // Slow-rate ON
                    "00000001" + // Slow Rate OFF
                    "";
                segs.Add(frm, String.Format("SG{0,2:d}",segCount++ % 100) + ln + LRC(ln));
                sseg.Add(frm);
            }
            // Now add the final one
            List<object> lseg = (List<Object>)lsegs.Last();
            long lfrm = (long)lseg[1];
            String lln = ((long)lseg[2]).ToString();
            segs.Add(lfrm, "STOP" + "1A0101001A000200000C"); // Tray stop command
            sseg.Add(lfrm);
            sseg.Sort();
        }

        public string LRC(String s) // Fucking redundant Longitudinal Redundancy Check (LRC) 
        {
            var lrc = 0;
            for (int i = 0; i < s.Length; i++)
                if (i<6 || i>=14) // Skip address and byte count (bastard)
                {
                    //int b = Convert.ToInt16(s.Substring(i, 2), 16);
                    int b = (int)s[i];
                    lrc = (lrc + b) & 0xFF;
                }
            lrc = ((lrc ^ 0xFF) + 1) & 0xFF;
            return string.Format("{0,2:X}", lrc).Replace(' ', '0');
        }
        public String whatDoWeDoNow(long seconds)
        {
            int current = sseg.BinarySearch(seconds);
            if (current >= 0)
                return segs[sseg[current]];
            current = -current-2;
            return segs[sseg[current]];
        }
    }
}
