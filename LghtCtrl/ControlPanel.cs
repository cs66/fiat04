﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;

namespace LghtCtrl
{
    public partial class ControlPanel : Form
    {
        // Globals
        // plan shows what trays we control and what recipes they are running
        Dictionary<String, Tray> plan;
        // recipes are from the web site or the cache
        Dictionary<String, Recipe> recipes = new Dictionary<String,Recipe>();
        public ControlPanel()
        {
            InitializeComponent();
        }

        private void ControlPanel_Load(object sender, EventArgs e)
        {
            // We start up without network first
            string[] ports = SerialPort.GetPortNames();
            foreach(String p in ports)
                SerialPortCBx.Items.Add(p);
            if (ports.Length == 0)
                SerialPortCBx.Text = "(none)";
            SerialPollCbx_CheckedChanged(sender, e);
            label8.Text = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+"\\IGSGS";
            if (!Directory.Exists(label8.Text + "\\RecipeCache"))
                Directory.CreateDirectory(label8.Text + "\\RecipeCache");
            try 
        	{
	            StreamReader sr = new StreamReader(
                label8.Text +
                "\\igsgs.ini");
                while (!sr.EndOfStream)
                {
                    String [] kv = sr.ReadLine().Split(new Char[] {':'},2);
                    if (kv[0] == "site")
                        SiteTbx.Text = kv[1].Trim();
                    if (kv[0] == "server")
                        ServerTbx.Text = kv[1].Trim();
                    if (kv[0] == "usr")
                        UsrTbx.Text = kv[1].Trim();
                    if (kv[0] == "pwd")
                        PwdTbx.Text = kv[1].Trim();
                    if (kv[0] == "serial" && SerialPortCBx.Items.Count>0)
                        SerialPortCBx.Text = kv[1].Trim();
                    if (kv[0] == "token")
                        SessionTokenTbx.Text = kv[1].Trim();
                }
                sr.Close();
                errLbx.Items.Add("ini successfully loaded");
	         }
            catch (Exception)
            {
                SiteTbx.Text = "(none)";
                ServerTbx.Text = "(none)";
                errLbx.Items.Add("ini failed to load");
            }
            LoadPlanBtn.PerformClick();
            if (PlanInPlaceLbl.BackColor == Color.Green)
            {
                SerialPollCbx.Checked = true;
                
            }
            // Having got the serial port working on local data we go to the controller for updates
            HTTP_StartSession.PerformClick();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(label8.Text +
                "\\igsgs.ini");
            sw.WriteLine("site:{0}", SiteTbx.Text);
            sw.WriteLine("server:{0}", ServerTbx.Text);
            sw.WriteLine("usr:{0}", UsrTbx.Text);
            sw.WriteLine("pwd:{0}", PwdTbx.Text);
            sw.WriteLine("serial:{0}", SerialPortCBx.Text);
            sw.WriteLine("token:{0}", SessionTokenTbx.Text);
            sw.Close();
        }


        int serialCounter = 0;
        Dictionary<long, String> currentValues = new Dictionary<long, string>();
        private void SerialTickerBtn_Click(object sender, EventArgs e)
        {
            GetPlanBtn_Click(null, null);
            serialCounter++;
            // For each tray in the plan, figure out what to do
            //:01___START_TRAY___
            //:01__WRITE_TO_TRAY_1A01010004002E000100000001000000000000003200320032000000640000000000000064006400640019000400000014000000143B
            foreach (Tray t in plan.Values)
            {
                if (!t.noRecipe)
                {
                    if (recipes.ContainsKey(t.recipe.ToString()))
                    {
                        Recipe r = recipes[t.recipe.ToString()];
                        long seconds = (long)(DateTime.Now - t.startAt).TotalSeconds;
                        String command = string.Format(":{0,2:X}", t.serialAddr).Replace(' ', '0') +
                            "        " +
                            string.Format("T{0,3:d}", serialCounter % 1000) +
                            r.whatDoWeDoNow(seconds);
                        if (currentValues.ContainsKey(t.serialAddr))
                        {
                            if (command.Substring(19) == currentValues[t.serialAddr].Substring(19))
                                continue;
                        }
                        currentValues[t.serialAddr] = command;
                        SendAndLogCmd(command);
                    }
                    else
                        errLbx.Items.Add("Could not get recipe " + t.recipe);
                }
            }
        }
        void SendAndLogCmd(String c)
        {
            if (SerialPortCBx.Text != "(none)")
            {
                if (!serialPort1.IsOpen)
                {
                    serialPort1.PortName = SerialPortCBx.Text;
                    serialPort1.BaudRate = 115200;
                    serialPort1.Open();
                }
                if (serialPort1.IsOpen)
                    serialPort1.Write(c);
            }
            SerialOutLbx.Items.Add(c);
            if (SerialOutLbx.Items.Count > 9)
                SerialOutLbx.TopIndex = SerialOutLbx.Items.Count - 10;
        }

        private void SerialPollCbx_CheckedChanged(object sender, EventArgs e)
        {
            SerialTmr.Enabled = SerialPollCbx.Checked;
        }

        private void ControlPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            Debug.WriteLine("Form Closed");
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            Debug.WriteLine("Data received on port:" + serialPort1.PortName);
            Debug.WriteLine(serialPort1.ReadExisting());
        }

        private void LoadPlanBtn_Click(object sender, EventArgs e)
        {//
            try
            {
                String path = label8.Text + "\\";
                Object o = JSONReader.ParseJSONFromFile(path + "plan.json");
                plan = Tray.jsonToPlan(o);
                Dictionary<String, bool> hasBeenRefreshed = new Dictionary<string, bool>();
                foreach (Tray t in plan.Values)
                {
                    if (!t.noRecipe && !hasBeenRefreshed.ContainsKey(t.recipe.ToString()))
                    {
                        recipes[t.recipe.ToString()] = new Recipe(path + "RecipeCache\\", t.recipe);
                        hasBeenRefreshed[t.recipe.ToString()] = true;
                    }
                }
                errLbx.Items.Add("local plan loaded");
                PlanInPlaceLbl.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                errLbx.Items.Add("Failed to parse local.json");
                PlanInPlaceLbl.BackColor = Color.Red;
                errLbx.Items.Add(ex.Message);
            }
        }

        private void HTTP_StartSession_Click(object sender, EventArgs e)
        {
            try
            {
                WebRequest req = WebRequest.Create(
                    String.Format("{0}?action=login&usr={1}&passwd={2}", ServerTbx.Text, UsrTbx.Text, PwdTbx.Text));
                HttpWebResponse rsp = (HttpWebResponse)req.GetResponse();
                StreamReader sr = new StreamReader(rsp.GetResponseStream());
                string responseFromServer = sr.ReadToEnd();
                Dictionary<String, object> auth = (Dictionary<String, Object>)JSONReader.ParseJSON(responseFromServer);
                SessionTokenTbx.Text = auth["token"].ToString();
                NetworkOKLbl.BackColor = Color.Green;
            }
            catch (Exception)
            {
                NetworkOKLbl.BackColor = Color.Red;
            }
        }

        private void GetPlanBtn_Click(object sender, EventArgs e)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(
                String.Format("{0}?action=get-site-settings&site={1}",
                    ServerTbx.Text,SiteTbx.Text
                    )
                );
            req.CookieContainer = new CookieContainer();
            req.CookieContainer.Add(
                new Uri(ServerTbx .Text),
                new Cookie("token",SessionTokenTbx.Text));
            req.CookieContainer.Add(
                new Uri(ServerTbx.Text),
                new Cookie("usr", UsrTbx.Text));
            HttpWebResponse rsp = (HttpWebResponse) req.GetResponse();
            String resp = (new StreamReader(rsp.GetResponseStream())).ReadToEnd();
            Object o = JSONReader.ParseJSON(resp);
            Dictionary<String,Object> dict = o as Dictionary<String,Object>;
            if (dict == null)
            {
                errLbx.Items.Add("Bad response from server at " + req.RequestUri);
                return;
            }
           
            if (dict.ContainsKey("err"))
            {
                errLbx.Items.Add(dict["err"]);
                return;
            }
            File.WriteAllText(label8.Text +
                "\\plan.json",
                resp);
            plan = Tray.jsonToPlan(o);
            Dictionary<String, Boolean> hasBeenRefreshed = new Dictionary<String, bool>();
            // TODO get refresh list from server to avoid downloading plans that have not changed
            string path = label8.Text +"\\RecipeCache\\";
            foreach (Tray t in plan.Values)
            {
                if (!hasBeenRefreshed.ContainsKey(t.recipe.ToString()) && !t.noRecipe)
                {
                    HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(
                        String.Format("{0}?action=get-recipe&rid={1}", ServerTbx.Text, t.recipe));
                    req1.CookieContainer = req.CookieContainer;
                    HttpWebResponse rsp1 = (HttpWebResponse)req1.GetResponse();
                    String resp1 = (new StreamReader(rsp1.GetResponseStream()).ReadToEnd());
                    if (!resp1.Contains("no such recipe"))
                    {
                        File.WriteAllText(path + t.recipe + ".json", resp1);
                        recipes[t.recipe.ToString()] = new Recipe(path, t.recipe);
                    }
                    hasBeenRefreshed.Add(t.recipe.ToString(), true);
                }
            }

        }

        private void ClearSerialListBtn_Click(object sender, EventArgs e)
        {
            currentValues = new Dictionary<long, string>();
        }
    }
}
